import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

/**
 * sound is not wotking ://
 * Created by Saeid Ghahremannejad on 6/10/2018.
 */
public class Sound {
    public static synchronized void playSound(final String p) {
        new Thread(new Runnable() {
            // The wrapper thread is unnecessary, unless it blocks on the
            // Clip finishing; see comments.
            public void run() {
                try {
                    Clip clip = AudioSystem.getClip();
                    AudioInputStream inputStream = AudioSystem.getAudioInputStream(StartMenu.class.getResourceAsStream(p));
                    clip.open(inputStream);
                    clip.start();
                    if (p.equals("src/sources/background.wav")){
                        clip.loop(2147483647);
                    }
                } catch (Exception e) {
                    System.out.println("hi");
                }
            }
        }).start();
    }
}
