/*** In The Name of Allah ***/


import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * The window on which the rendering is performed.
 * This example uses the modern BufferStrategy approach for double-buffering,
 * actually it performs triple-buffering!
 * For more information on BufferStrategy check out:
 * http://docs.oracle.com/javase/tutorial/extra/fullscreen/bufferstrategy.html
 * http://docs.oracle.com/javase/8/docs/api/java/awt/image/BufferStrategy.html
 *
 * @author Seyed Mohammad Ghaffarian
 */
public class GameFrame extends JFrame {

    public static final int GAME_HEIGHT = 1080;                  // 720p game resolution
    public static final int GAME_WIDTH = 16 * GAME_HEIGHT / 9;  // wide aspect ratio


    //uncomment all /*...*/ in the class for using Tank icon instead of a simple circle

    public static ImageIcon cur = new ImageIcon("src/sources/cur.png");
    private long lastRender;
    private ArrayList<Float> fpsHistory;
    static long finishTime;
    static int s = 0;
    private BufferStrategy bufferStrategy;

    ///////////////////////////////////////////////////////////////
    public static BufferedImage soil, tree0, tree1, tree2, route, finish, enemy, imgTnk, imgGun, imgFire2, imgGun2, imgFire, imgExplosion, imgExplosion2, imgMissile, softBrick1, softBrick2, softBrick3, mgunFood, missileFood, repairFood, upgradeFood, bullet;
    public static BufferedImage enemyTank, enemyTankGun, enemyFixedTank, enemyFixedTankGun, launcher, launcherGun, missilesLeft, bulletsLeft;


    //////////////////////////////////////////////////////////////

    public GameFrame(String title) {
        super(title);
        setResizable(false);
        setSize(GAME_WIDTH, GAME_HEIGHT);
        lastRender = -1;
        fpsHistory = new ArrayList<>(100);

        try {
            imgTnk = ImageIO.read(new File("src/sources/tank.png"));
            setCursor(Toolkit.getDefaultToolkit().createCustomCursor(cur.getImage(), new Point(0, 0), "custom cursor"));
            soil = ImageIO.read(new File("src/sources/soil.jpg"));
            //    tree0 = ImageIO.read(new File("C:\\Users\\user-pc\\Desktop\\AP Final\\JTanks\\Incremental\\src\\source\\tree.png"));
            //    tree1 = ImageIO.read(new File("C:\\Users\\user-pc\\Desktop\\AP Final\\JTanks\\Incremental\\src\\source\\tree1.png"));
            //    tree2 = ImageIO.read(new File("C:\\Users\\user-pc\\Desktop\\AP Final\\JTanks\\Incremental\\src\\source\\tree2.png"));
            route = ImageIO.read(new File("src/sources/ground.png"));
            finish = ImageIO.read(new File("src/sources/finish.png"));
            enemy = ImageIO.read(new File("src/sources/enemy.png"));
            imgGun = ImageIO.read(new File("src/sources/tankGun01.png"));
            imgGun2 = ImageIO.read(new File("src/sources/tankGun02.png"));
            imgMissile = ImageIO.read(new File("src/sources/missile.png"));
            softBrick1 = ImageIO.read(new File("src/sources/softWall1.png"));
            softBrick2 = ImageIO.read(new File("src/sources/softWall2.png"));
            softBrick3 = ImageIO.read(new File("src/sources/softWall3.png"));
            bullet = ImageIO.read(new File("src/sources/LightBullet.png"));
            imgFire = ImageIO.read(new File("src/sources/fire.png"));
            imgFire2 = ImageIO.read(new File("src/sources/fire2.png"));
            imgExplosion = ImageIO.read(new File("src/sources/imgExplosion.png"));
            imgExplosion2 = ImageIO.read(new File("src/sources/imgExplosion2.png"));
            enemyTank = ImageIO.read(new File("src/sources/enemyTank.png"));
            enemyTankGun = ImageIO.read(new File("src/sources/enemyTankGun.png"));
            enemyFixedTank = ImageIO.read(new File("src/sources/fixedtank.png"));
            enemyFixedTankGun = ImageIO.read(new File("src/sources/fixedtankgun.png"));
            launcher = ImageIO.read(new File("src/sources/launcher.png"));
            launcherGun = ImageIO.read(new File("src/sources/launcherGun.png"));
            mgunFood = ImageIO.read(new File("src/sources/MashinGunFood.png"));
            missileFood = ImageIO.read(new File("src/sources/CannonFood.png"));
            repairFood = ImageIO.read(new File("src/sources/RepairFood.png"));
            upgradeFood = ImageIO.read(new File("src/sources/upgrader.png"));
            missilesLeft = ImageIO.read(new File("src/sources/NumberOfHeavyBullet2.png"));
            bulletsLeft = ImageIO.read(new File("src/sources/NumberOfMachinGun2.png"));


        } catch (IOException e) {
            System.out.println(e);
        }

        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setUndecorated(true);

    }

    /**
     * This must be called once after the JFrame is shown:
     * frame.setVisible(true);
     * and before any rendering is started.
     */
    public void initBufferStrategy() {
        // Triple-buffering
        createBufferStrategy(5);
        bufferStrategy = getBufferStrategy();
    }


    /**
     * Game rendering with triple-buffering using BufferStrategy.
     */
    public void render(GameState state) {
        // Render single frame
        do {
            // The following loop ensures that the contents of the drawing buffer
            // are consistent in case the underlying surface was recreated
            do {
                // Get a new graphics context every time through the loop
                // to make sure the strategy is validated
                Graphics2D graphics = (Graphics2D) bufferStrategy.getDrawGraphics();
                try {
                    doRendering(graphics, state);
                } finally {
                    // Dispose the graphics
                    graphics.dispose();
                }
                // Repeat the rendering if the drawing buffer contents were restored
            } while (bufferStrategy.contentsRestored());

            // Display the buffer
            bufferStrategy.show();
            // Tell the system to do the drawing NOW;
            // otherwise it can take a few extra ms and will feel jerky!
            Toolkit.getDefaultToolkit().sync();

            // Repeat the rendering if the drawing buffer was lost
        } while (bufferStrategy.contentsLost());
    }

    /**
     * Rendering all game elements based on the game state.
     */
    static int transY = 0;
    static int transX = 0;

    private void doRendering(Graphics2D g2d, GameState state) {
        g2d.translate(transX, transY);
        // Draw background
        //g2d.drawImage(route, 200, 400, null);

        //render game env
        for (Sprite s : CollideAssist.gameObjetcs
                ) {
            switch (s.getType()) {
                case Sprite.BRICK:
                    g2d.drawImage(soil, s.getX(), s.getY(), null);
                    break;
                case Sprite.ROUTE:
                    g2d.drawImage(route, s.getX(), s.getY(), null);
                    break;
                case Sprite.TANK:
                    g2d.drawImage(route, s.getX(), s.getY(), null);
                    break;
                case Sprite.SOFT_BRICK:
                    g2d.drawImage(route, s.getX(), s.getY(), null);

                    break;
                case Sprite.IDIOT_ENEMY:
                    g2d.drawImage(route, s.getX(), s.getY(), null);
                    break;
                case Sprite.TANK_ENEMY:
                    g2d.drawImage(route, s.getX(), s.getY(), null);
                    break;
                case Sprite.FIXED_ENEMY:
                    g2d.drawImage(route, s.getX(), s.getY(), null);
                    break;
                case Sprite.LAUNCHER:
                    g2d.drawImage(route, s.getX(), s.getY(), null);
                    break;
                case Sprite.FOOD_REPAIR:
                    g2d.drawImage(route, s.getX(), s.getY(), null);

                    break;
                case Sprite.FOOD_G:
                    g2d.drawImage(route, s.getX(), s.getY(), null);

                    break;
                case Sprite.FOOD_M:
                    g2d.drawImage(route, s.getX(), s.getY(), null);

                    break;
                case Sprite.FOOD_UPGRADE:
                    g2d.drawImage(route, s.getX(), s.getY(), null);

                    break;
                case Sprite.FOOD_BRICK:
                    g2d.drawImage(route, s.getX(), s.getY(), null);
                    break;


            }
        }
        /**
         * render foods
         */
        for (Sprite s : GameState.FOOD
                ) {
            switch (s.getType()) {
                case Sprite.FOOD_REPAIR:

                    g2d.drawImage(repairFood, s.getX(), s.getY(), null);
                    break;
                case Sprite.FOOD_G:

                    g2d.drawImage(mgunFood, s.getX(), s.getY(), null);
                    break;
                case Sprite.FOOD_M:

                    g2d.drawImage(missileFood, s.getX(), s.getY(), null);
                    break;
                case Sprite.FOOD_UPGRADE:

                    g2d.drawImage(upgradeFood, s.getX(), s.getY(), null);
                    break;
            }
        }
        /**
         * render soft bricks
         */
        for (Sprite s : GameState.GAME_SOFTBRICKS
                ) {
            if (s.isVisible) {
                g2d.drawImage(route, s.getX(), s.getY(), null);
                if ((int) s.life == 3) {
                    g2d.drawImage(softBrick1, s.getX(), s.getY(), null);
                }
                if ((int) s.life == 2) {
                    g2d.drawImage(softBrick2, s.getX(), s.getY(), null);
                }
                if ((int) s.life == 1) {
                    g2d.drawImage(softBrick3, s.getX(), s.getY(), null);
                }
            } else {
                g2d.drawImage(route, s.getX(), s.getY(), null);
            }
        }
        for (Enemy s : GameState.IDIOT_ENEMY) {
            if (s.isVisible) {
                //g2d.drawImage(route,s.getX(), s.getY(), null);
                g2d.drawImage(enemy, s.getX(), s.getY(), null);
            } else {
                g2d.drawImage(route, s.getX(), s.getY(), null);
            }
        }
        /**
         * render tank enemies
         */
        for (Enemy s : GameState.TANK_ENEMY
                ) {
            if (s.isVisible) {
                AffineTransform old = g2d.getTransform();
                AffineTransform at = new AffineTransform();
                //g2d.drawImage(route,s.getX(), s.getY(), null);
                at.rotate(s.theta, s.getX() - GameState.xScroll + enemyTank.getWidth() / 2, s.getY() - GameState.yScroll + enemyTank.getHeight() / 2);
                g2d.setTransform(at);
                g2d.drawImage(enemyTank, s.getX() - GameState.xScroll, s.getY() - GameState.yScroll, null);


                g2d.setTransform(old);
                at = new AffineTransform();
                at.rotate(s.alpha, s.getX() - GameState.xScroll + enemyTank.getWidth() / 2, s.getY() - GameState.yScroll + enemyTank.getHeight() / 2);
                g2d.setTransform(at);
                g2d.drawImage(enemyTankGun, s.getX() - GameState.xScroll + enemyTank.getWidth() / 2 - enemyTankGun.getWidth() / 2, s.getY() - GameState.yScroll + enemyTank.getHeight() / 2 - enemyTankGun.getHeight() / 2, null);
                g2d.setTransform(old);
            } else {
                g2d.drawImage(route, s.getX(), s.getY(), null);
            }
        }
        /**
         * reder finish vector
         */
        g2d.drawImage(route, GameState.finish.getX(), GameState.finish.getY(), null);
        g2d.drawImage(finish, GameState.finish.getX(), GameState.finish.getY(), null);

        /**
         * render enemy fixed tanks
         */
        for (Enemy s : GameState.FIXED_TANK
                ) {
            if (s.isVisible) {
                //g2d.drawImage(route,s.getX(), s.getY(), null);
                g2d.drawImage(enemyFixedTank, s.getX(), s.getY(), null);
                AffineTransform old = g2d.getTransform();
                AffineTransform at = new AffineTransform();
                at.rotate(s.alpha, s.getX() - GameState.xScroll + enemyFixedTank.getWidth() / 2, s.getY() - GameState.yScroll + enemyFixedTank.getHeight() / 2);
                g2d.setTransform(at);
                g2d.drawImage(enemyFixedTankGun, s.getX() - GameState.xScroll + enemyFixedTank.getWidth() / 2 - enemyFixedTankGun.getWidth() / 2, s.getY() - GameState.yScroll + enemyFixedTank.getHeight() / 2 - enemyFixedTankGun.getHeight() / 2, null);
                g2d.setTransform(old);
            } else {
                g2d.drawImage(route, s.getX(), s.getY(), null);
            }
        }
        /**
         * render enemy launchers
         */
        for (Enemy s : GameState.LAUNCHER
                ) {
            if (s.isVisible) {
                //g2d.drawImage(route,s.getX(), s.getY(), null);
                g2d.drawImage(launcher, s.getX(), s.getY(), null);
                AffineTransform old = g2d.getTransform();
                AffineTransform at = new AffineTransform();
                at.rotate(s.alpha, s.getX() - GameState.xScroll + launcher.getWidth() / 2, s.getY() - GameState.yScroll + launcher.getHeight() / 2);
                g2d.setTransform(at);
                g2d.drawImage(launcherGun, s.getX() - GameState.xScroll + launcher.getWidth() / 2 - launcherGun.getWidth() / 2, s.getY() - GameState.yScroll + launcher.getHeight() / 2 - launcherGun.getHeight() / 2, null);
                g2d.setTransform(old);


                //  g2d.drawImage(enemyTankGun, s.getX() - GameState.xScroll + enemyTank.getWidth() / 2 - enemyTankGun.getWidth() / 2, s.getY() - GameState.yScroll + enemyTank.getHeight() / 2 - enemyTankGun.getHeight() / 2, null);


            } else {
                g2d.drawImage(route, s.getX(), s.getY(), null);
            }
        }


        /**
         * to handle tank and gun rotation
         */
        AffineTransform old = g2d.getTransform();
        AffineTransform at = new AffineTransform();
        //at.setToScale(0.85, 0.85);
        at.rotate((state.tank.theta), imgTnk.getWidth() / 2 + state.tank.x, imgTnk.getHeight() / 2 + state.tank.y);
        g2d.setTransform(at);

        g2d.drawImage(imgTnk, state.tank.x, state.tank.y, null);


        g2d.setTransform(old);


        if (GameState.isMultiplayer) {
            at = new AffineTransform();
            //at.setToScale(0.85, 0.85);
            at.rotate((state.friendTank.theta), imgTnk.getWidth() / 2 + state.friendTank.x, imgTnk.getHeight() / 2 + state.friendTank.y);
            g2d.setTransform(at);

            g2d.drawImage(imgTnk, state.friendTank.x, state.friendTank.y, null);


            g2d.setTransform(old);
        }
        for (Missile ms : state.missiles
                ) {
            if (ms.isVisible()) {
                at = new AffineTransform();
                at.rotate(ms.angel, ms.xC, ms.yC);
                g2d.setTransform(at);
                if (ms.type == GameState.MISSILE_ACTIVE) {
                    g2d.drawImage(imgMissile, ms.getX(), ms.getY(), this);
                } else {
                    g2d.drawImage(bullet, ms.getX(), ms.getY(), this);
                }
                // System.out.println(state.missiles.get(0).x + "    " + state.missiles.get(0).getX() );
                g2d.setTransform(old);
                //System.out.println( "      " +state.missiles.get(0).x + "    " + state.missiles.get(0).getX() );

            }
        }

        at = new AffineTransform();
        // at.setToScale(0.85, 0.85);


        at.rotate(GameState.tank.alpha, imgTnk.getWidth() / 2 + state.tank.x, imgTnk.getHeight() / 2 + state.tank.y);
        g2d.setTransform(at);

        if (state.tank.getActiveGun() == Tank.MISSILE) {

            if (GameState.firedAnim && state.tank.getMissilesLeft() > 0) {
                g2d.drawImage(imgFire2, state.tank.x + imgGun2.getWidth(), state.tank.y - imgFire2.getHeight() / 2 + imgTnk.getHeight() / 2, null);
            }
            g2d.drawImage(imgGun, state.tank.x - imgGun.getWidth() / 2 + imgTnk.getWidth() / 2 - GameState.fired * 5, state.tank.y - imgGun.getHeight() / 2 + imgTnk.getHeight() / 2, null);
        } else {
            if (GameState.firedAnim && state.tank.getBulletsLeft() > 0) {
                g2d.drawImage(imgFire, state.tank.x + imgGun2.getWidth(), state.tank.y - imgFire.getHeight() / 2 + imgTnk.getHeight() / 2, null);
            }
            g2d.drawImage(imgGun2, state.tank.x - imgGun2.getWidth() / 2 + imgTnk.getWidth() / 2 - GameState.fired * 5, state.tank.y - imgGun2.getHeight() / 2 + imgTnk.getHeight() / 2, null);
        }


        g2d.setTransform(old);

        /**
         * rotation in multiplayer mode
         */
        if (GameState.isMultiplayer) {
            at = new AffineTransform();
            // at.setToScale(0.85, 0.85);


            at.rotate(GameState.friendTank.alpha, imgTnk.getWidth() / 2 + state.friendTank.x, imgTnk.getHeight() / 2 + state.friendTank.y);
            g2d.setTransform(at);

            if (state.friendTank.activeGunn == Tank.MISSILE) {

                if ( state.friendTank.getMissilesLeft() > 0) {
                    // g2d.drawImage(imgFire2, state.friendTank.x + imgGun2.getWidth(), state.friendTank.y - imgFire2.getHeight() / 2 + imgTnk.getHeight() / 2, null);
                }
                g2d.drawImage(imgGun, state.friendTank.x - imgGun.getWidth() / 2 + imgTnk.getWidth() / 2 - GameState.fired * 0, state.friendTank.y - imgGun.getHeight() / 2 + imgTnk.getHeight() / 2, null);
            } else {
                if ( state.friendTank.getBulletsLeft() > 0) {
                    // g2d.drawImage(imgFire, state.friendTank.x + imgGun2.getWidth(), state.friendTank.y - imgFire.getHeight() / 2 + imgTnk.getHeight() / 2, null);
                }
                g2d.drawImage(imgGun2, state.friendTank.x - imgGun2.getWidth() / 2 + imgTnk.getWidth() / 2 - GameState.fired * 0, state.friendTank.y - imgGun2.getHeight() / 2 + imgTnk.getHeight() / 2, null);
            }


            g2d.setTransform(old);
        }


        for (Explosion ex : GameState.explosions) {
            if (ex.active) {
                if (ex.type == GameState.BULLETS_ACTIVE)
                    g2d.drawImage(GameFrame.imgExplosion, ex.x - imgExplosion.getWidth() / 2, ex.y - imgExplosion.getHeight() / 2, null);
                if (ex.type == GameState.MISSILE_ACTIVE)
                    g2d.drawImage(GameFrame.imgExplosion2, ex.x - imgExplosion2.getWidth() / 2, ex.y - imgExplosion2.getHeight() / 2, null);
            }
        }
        // g2d.drawLine(imgTnk.getWidth() / 2 + state.tank.x, imgTnk.getHeight() / 2 + state.tank.y,state.mouseX,state.mouseY);


        g2d.setColor(new Color(109, 135, 100));
        g2d.fillRect(GameState.xScroll, GameState.yScroll, 1920, 60);

        g2d.setColor(Color.GREEN);
        g2d.setFont(new Font("Sans Serif", Font.BOLD, 20));
        g2d.drawImage(missilesLeft, GameState.xScroll + 100, GameState.yScroll, null);
        g2d.drawString("" + state.tank.getMissilesLeft(), GameState.xScroll + 150, GameState.yScroll + 40);

        g2d.drawImage(bulletsLeft, GameState.xScroll + 300, GameState.yScroll - 10, null);
        g2d.drawString("" + state.tank.getBulletsLeft(), GameState.xScroll + 350, GameState.yScroll + 40);


        g2d.drawRect(GAME_WIDTH / 2 - 200 + GameState.xScroll, 8 + GameState.yScroll, 400, 45);

        g2d.fillRect(GAME_WIDTH / 2 - 200 + GameState.xScroll, 8 + GameState.yScroll, (int) (state.tank.getHitPoint() / 5 * 400) > 0 ? (int) (state.tank.getHitPoint() / 5 * 400) : 0, 45);

        /**
         * pause game
         */
        if (state.isPaused) {
            g2d.setColor(Color.WHITE);
            g2d.setFont(new Font("Sans Serif", Font.PLAIN, 100));
            g2d.drawString("PAUSED", 670 + GameState.xScroll, 1080 / 2 + GameState.yScroll);
        }
        else if (state.isFriendPaused) {
            g2d.setColor(Color.WHITE);
            g2d.setFont(new Font("Sans Serif", Font.PLAIN, 100));
            g2d.drawString("FRIEND PAUSED GAME", 470 + GameState.xScroll, 1080 / 2 + GameState.yScroll);
        }
        /**
         * drw game over
         */
        if (state.loose) {
            g2d.setColor(Color.WHITE);
            g2d.setFont(new Font("Sans Serif", Font.PLAIN, 100));
            g2d.drawString("GAME OVER", 670 + GameState.xScroll, 1080 / 2 + GameState.yScroll);
        }
        /**
         * draw well done
         */
        if (state.isFinished) {
            g2d.setColor(Color.WHITE);
            g2d.setFont(new Font("Sans Serif", Font.PLAIN, 100));

            g2d.drawString("WELL DONE", 670 + GameState.xScroll, 1080 / 2 + GameState.yScroll);
            if (s == 0) {
                s++;
                finishTime = System.currentTimeMillis();
                //System.out.println(finishTime);
            }
            //  System.out.println(finishTime + "         " + System.currentTimeMillis());
            if (finishTime + 2000 < System.currentTimeMillis())
                dispose();


        }

    }
}