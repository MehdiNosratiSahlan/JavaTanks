import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;

/**
 * settings page of the game which contains game settings
 */

public class Settings extends JFrame {
    JLabel sound, back, map, level;
    JFileChooser chooser;
JPanel sets;
 static File mapFile;
    public Settings() {
        super("Settings");

        setSize(600, 800);
        setUndecorated(true);
        JPanel sPanel = new JPanel(new BorderLayout(5, 5));
        setLocationRelativeTo(null);
        sPanel.setBackground(new Color(160, 82, 45));
        setContentPane(sPanel);
        sound = new JLabel("                  SOUND is ON");
        sound.setForeground(new Color(100, 118, 135));
        sound.setFont(new Font("Agency FB", Font.BOLD, 50));
        sound.addMouseListener(new mouseEvents());

        back = new JLabel("                        Back");
        back.setForeground(new Color(100, 118, 135));
        back.setFont(new Font("Agency FB", Font.BOLD, 50));
        back.addMouseListener(new mouseEvents());

        sets = new JPanel(new GridLayout(4, 1, 20, 70));

        map = new JLabel("                 map generator");
        map.setForeground(new Color(100, 118, 135));
        map.setFont(new Font("Agency FB", Font.BOLD, 50));
        map.addMouseListener(new mouseEvents());

        level = new JLabel("                         levels");
        level.setForeground(new Color(100, 118, 135));
        level.setFont(new Font("Agency FB", Font.BOLD, 50));
        level.addMouseListener(new mouseEvents());

        sets.add(sound);
        sets.add(map);
        sets.add(level);
        sets.add(back);

        sets.setBackground(new Color(160, 82, 45));
        sPanel.add(sets, BorderLayout.NORTH);
        setVisible(true);


    }


    private class mouseEvents implements MouseListener {
        @Override
        public void mouseClicked(MouseEvent e) {
            if (e.getSource().equals(sound) && sound.getText().contains("ON")) {
                sound.setText("                 SOUND is OFF");
                StartMenu.soundOn = false;
            } else if (e.getSource().equals(sound) && sound.getText().contains("OFF")) {
                sound.setText("                 SOUND is ON");
                StartMenu.soundOn = true;
            } else if (e.getSource().equals(back)) {
                dispose();
            } else if (e.getSource().equals(map)) {
                MainMap.main(null);
            }
            else if (e.getSource().equals(level)) {
                if (chooser == null) {
                    chooser = new JFileChooser();
                    chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
                    chooser.setAcceptAllFileFilterUsed(false);
                    chooser.addChoosableFileFilter(new FileFilter() {
                        @Override
                        public boolean accept(File f) {
                            return f.isDirectory() || f.getName().toLowerCase().endsWith(".txt");
                        }

                        @Override
                        public String getDescription() {
                            return "Text Files (*.txt)";
                        }
                    });

                    switch (chooser.showOpenDialog(sets)){
                        case JFileChooser.APPROVE_OPTION:
                            MapUtils.difficulty = 3;
                            mapFile = chooser.getSelectedFile();
                            MapUtils mapUtils = new MapUtils();
                            Main.main(null);
                            break;

                    }

                }

            }
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            if (e.getSource().equals(sound)) {
                sound.setForeground(new Color(240, 163, 10));
            } else if (e.getSource().equals(back)) {
                back.setForeground(new Color(240, 163, 10));
            } else if (e.getSource().equals(map)) {
                map.setForeground(new Color(240, 163, 10));
            }else if (e.getSource().equals(level)) {
                level.setForeground(new Color(240, 163, 10));
            }
        }

        @Override
        public void mouseExited(MouseEvent e) {
            if (e.getSource().equals(sound)) {
                sound.setForeground(new Color(100, 118, 135));
            } else if (e.getSource().equals(back)) {
                back.setForeground(new Color(100, 118, 135));
            } else if (e.getSource().equals(map)) {
                map.setForeground(new Color(100, 118, 135));
            }else if (e.getSource().equals(level)) {
                level.setForeground(new Color(100, 118, 135));
            }
        }

        @Override
        public void mouseReleased(MouseEvent e) {

        }

        @Override
        public void mousePressed(MouseEvent e) {

        }

    }
}
