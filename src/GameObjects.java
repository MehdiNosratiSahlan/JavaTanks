import java.io.Serializable;
import java.util.ArrayList;

/**
 * for save load
 *
 * Created by Saeid Ghahremannejad on 7/13/2018.
 */
public class GameObjects implements Serializable {


    public ArrayList<Sprite> bricks;
    public ArrayList<Sprite> softBricks;
    public Tank tank;
    public Sprite finish;
    public ArrayList<Enemy> idiotEnemies;
    public ArrayList<Enemy> tankEnemies;
    public ArrayList<Missile> missiles;
    public ArrayList<Enemy> fixedTanks;
    public ArrayList<Explosion> explosions;
    public  ArrayList<Enemy> launcher;
    public  ArrayList<Sprite> food;
    public  ArrayList<Sprite> gObjects;
    public int xScroll,yScroll;

    public GameObjects(){
        bricks = new ArrayList<>(GameState.GAME_BRICKS);
        softBricks = new ArrayList<>(GameState.GAME_SOFTBRICKS);
        tank = GameState.tank;
        idiotEnemies = new ArrayList<>(GameState.IDIOT_ENEMY);
        tankEnemies = new ArrayList<>(GameState.TANK_ENEMY);
        missiles = new ArrayList<>(GameState.missiles);
        fixedTanks = new ArrayList<>(GameState.FIXED_TANK);
        explosions = new ArrayList<>(GameState.explosions);
        launcher = new ArrayList<>(GameState.LAUNCHER);
        food = new ArrayList<>(GameState.FOOD);
        gObjects = new ArrayList<>(CollideAssist.gameObjetcs);
        finish = GameState.finish;
        xScroll = GameState.xScroll;
        yScroll = GameState.yScroll;

    }
}
