/*** In The Name of Allah ***/


import javax.swing.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;

/*
 * This class holds the state of game and all of its elements.
 * This class also handles user inputs, which affect the game state.
 * 
 * @author Seyed Mohammad Ghaffarian
 */
public class GameStateMap {
	
	public int locX, locY, diam;
	public boolean gameOver;
	public static char ch = 'S';
	private boolean keyUP, keyDOWN, keyRIGHT, keyLEFT;
	private boolean mousePress;
	public int mouseX, mouseY;
	private KeyHandler keyHandler;
	private MouseHandler mouseHandler;
	Vector<Character> map = new Vector<>();
	int x = 30;
	int y = 25;

    public static int xScroll = 0;
    public static int yScroll = 0;
	
	public GameStateMap() {

		locX = 100;
		locY = 100;
		diam = 32;
		gameOver = false;
		//
		keyUP = false;
		keyDOWN = false;
		keyRIGHT = false;
		keyLEFT = false;
		//
		mousePress = false;
		mouseX = 0;
		mouseY = 0;
		//
		keyHandler = new KeyHandler();
		mouseHandler = new MouseHandler();
		for (int i = 0 ; i < y*x ; i++){
            map.add(i,'O');
        }
       // map.set(3,'E');
       // System.out.println(map.toString());
    }
	
	/**
	 * The method which updates the game state.
	 */
	public void update() {
//        System.out.println(a[1][1]);



        if (mouseX > GameFrameMap.GAME_WIDTH/10*9 && xScroll*(-1) < x*100 + 150 - GameFrameMap.GAME_WIDTH){
             xScroll -=5;
           // System.out.println(xScroll);
        }
        if (mouseX < GameFrameMap.GAME_WIDTH/10*1 + 150 && mouseX>150 && xScroll*(-1)>0){
            xScroll +=5;
        }
        if (mouseY > GameFrameMap.GAME_HEIGHT/10*9 && yScroll*(-1) < y*100 + 100 - GameFrameMap.GAME_HEIGHT){
            yScroll -=5;
           // System.out.println(xScroll);
        }
        if (mouseY < GameFrameMap.GAME_HEIGHT/10*1 + 100 && mouseY>100 && yScroll*(-1)>0){
            yScroll +=5;
        }
	}
	
	
	public KeyListener getKeyListener() {
		return keyHandler;
	}
	public MouseListener getMouseListener() {
		return mouseHandler;
	}
	public MouseMotionListener getMouseMotionListener() {
		return mouseHandler;
	}


	public void save(){
		String name = JOptionPane.showInputDialog("map name: ",JOptionPane.QUESTION_MESSAGE);
		name = "src/sources/MapGen/" +  name + ".txt";

		BufferedWriter writer = null;
		File mapFile = new File(name);
		if (!mapFile.exists()){
			try {
				mapFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			writer = new BufferedWriter(new FileWriter(mapFile));

			for (int i = 0 ; i < y ; i++){
				for (int j = 0; j < x; j++) {
					writer.write(map.get(i * x + j));
				}
				writer.newLine();
			}

			writer.close();


		} catch (IOException e) {
			e.printStackTrace();
		}


	}

	/**
	 * The keyboard handler.
	 */
	class KeyHandler extends KeyAdapter {

		@Override
		public void keyPressed(KeyEvent e) {
			switch (e.getKeyCode())
			{
				case KeyEvent.VK_UP:
					keyUP = true;
					break;
				case KeyEvent.VK_DOWN:
					keyDOWN = true;
					break;
				case KeyEvent.VK_LEFT:
					keyLEFT = true;
					break;
				case KeyEvent.VK_RIGHT:
					keyRIGHT = true;
					break;
				case KeyEvent.VK_ESCAPE:
					gameOver = true;
					break;
			}
			if (e.getKeyCode() == KeyEvent.VK_S && e.isControlDown()){
				save();
			}
		}

		@Override
		public void keyReleased(KeyEvent e) {
			switch (e.getKeyCode())
			{
				case KeyEvent.VK_UP:
					keyUP = false;
					break;
				case KeyEvent.VK_DOWN:
					keyDOWN = false;
					break;
				case KeyEvent.VK_LEFT:
					keyLEFT = false;
					break;
				case KeyEvent.VK_RIGHT:
					keyRIGHT = false;
					break;
			}
		}

	}

	/**
	 * The mouse handler.
	 */
	class MouseHandler extends MouseAdapter {
        @Override
        public void mouseMoved(MouseEvent e){
            mouseX = e.getX();
            mouseY = e.getY();

        }
		@Override
		public void mousePressed(MouseEvent e) {
			mouseX = e.getX();
            mouseY = e.getY();
			if (mouseX > 150 && mouseY>100) {
				if ((mouseX - xScroll - 150) >= 0 && (mouseX - xScroll - 150) / 100 < x && (mouseY - yScroll - 100) >= 0 && (mouseY - yScroll - 100) / 100 < y) {
					if (ch == 'T') {
						for (int i = 0; i < map.size(); i++) {
							if (map.get(i) == 'T')
								map.set(i, 'O');
						}

					}

					if (ch == 'F') {
						for (int i = 0; i < map.size(); i++) {
							if (map.get(i) == 'F')
								map.set(i, 'O');
						}

					}

					map.set(((mouseY - yScroll - 100) / 100) * x + (mouseX - xScroll - 150) / 100, ch);
				}
				mousePress = true;
			}
			if (mouseX>=25 && mouseX<=65 && mouseY>=110 &&mouseY<=150){
				ch = 'S';
			}
			if (mouseX>=85 && mouseX<=125 && mouseY>=110 &&mouseY<=150){
				ch = 'O';
			}
			if (mouseX>=25 && mouseX<=65 && mouseY>=170 &&mouseY<=210){
				ch = 'b';
			}


			if (mouseX>=85 && mouseX<=125 && mouseY>=170 &&mouseY<=210){

				ch = 'T';
			}



			if (mouseX>=25 && mouseX<=65 && mouseY>=230 &&mouseY<=270){
				ch = 'e';
			}


			if (mouseX>=85 && mouseX<=125 && mouseY>=230 &&mouseY<=270){
				ch = 'L';
			}



			if (mouseX>=25 && mouseX<=65 && mouseY>=290 &&mouseY<=330){
				ch = 'i';
			}


			if (mouseX>=85 && mouseX<=125 && mouseY>=290 &&mouseY<=330){
				ch = 'l';
			}


			if (mouseX>=25 && mouseX<=65 && mouseY>=350 &&mouseY<=390){
				ch = 'u';
			}


			if (mouseX>=85 && mouseX<=125 && mouseY>=350 &&mouseY<=390){
				ch = 'U';
			}

			if (mouseX>=25 && mouseX<=65 && mouseY>=410 &&mouseY<=450){
				ch = 'r';
			}


			if (mouseX>=85 && mouseX<=125 && mouseY>=410 &&mouseY<=450){
				ch = 'R';
			}
			if (mouseX>=25 && mouseX<=65 && mouseY>=470 &&mouseY<=510){
				ch = 'F';
			}

		}

		@Override
		public void mouseReleased(MouseEvent e) {
			mousePress = false;
		}

		@Override
		public void mouseDragged(MouseEvent e) {
			mouseX = e.getX();
			mouseY = e.getY();

			if ((mouseX - xScroll - 150) >= 0 && (mouseX - xScroll - 150) / 100 < x && (mouseY - yScroll - 100) >= 0 && (mouseY - yScroll - 100) / 100 < y){
				if (ch != 'T') {
					map.set(((mouseY - yScroll - 100) / 100) * x + (mouseX - xScroll - 150) / 100, ch);
				}else
				{
					for (int i =0 ; i<map.size() ; i++){
						if (map.get(i) == 'T')
							map.set(i, 'O');
					}
					map.set(((mouseY - yScroll - 100) / 100) * x + (mouseX - xScroll - 150) / 100, ch);
				}
				if (ch != 'F') {
					map.set(((mouseY - yScroll - 100) / 100) * x + (mouseX - xScroll - 150) / 100, ch);
				}else
				{
					for (int i =0 ; i<map.size() ; i++){
						if (map.get(i) == 'F')
							map.set(i, 'O');
					}
					map.set(((mouseY - yScroll - 100) / 100) * x + (mouseX - xScroll - 150) / 100, ch);
				}
			}
			mousePress = true;
//            if (mouseX>=25 && mouseX<=65 && mouseY>=100 &&mouseY<=140){
//                ch = 'S';
//            }
//            if (mouseX>=85 && mouseX<=125 && mouseY>=100 &&mouseY<=140){
//                ch = 'O';
//            }
//            if (mouseX>=25 && mouseX<=65 && mouseY>=160 &&mouseY<=200){
//                ch = 'B';
//            }
		}
	}
}

