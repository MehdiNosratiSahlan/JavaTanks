/**
 * Tank class which will handle all logical information about the main char of the game
 *
 * @author MnS
 */


public class Tank extends Sprite {
    /**
     * rotations
     */
    public double theta, gama,alpha;
    /**
     * shooting type
     */
    public static final int MISSILE = 0, MACHINE_GUN = 1;
    /**
     * shooting limit
     */
    private int missilesLeft = 100, bulletsLeft = 200;
    public static int activeGun = 0;
    public int activeGunn = 0;
    /**
     * death detect
     */
    private double hitPoint = 5;

    public double getHitPoint() {
        return hitPoint;
    }

    public void setHitPoint(double hitPoint) {
        this.hitPoint = hitPoint;
    }

    public static int getActiveGun() {
        return activeGun;
    }

    public static void setActiveGun(int activGun) {
        activeGun = activGun;
    }

    public Tank(){
        super(0,0, Sprite.TANK);


    }


    public int getMissilesLeft() {
        return missilesLeft;
    }

    public void setMissilesLeft(int missilesLeft) {
        this.missilesLeft = missilesLeft;
    }

    public int getBulletsLeft() {
        return bulletsLeft;
    }

    public void setBulletsLeft(int bulletsLeft) {
        this.bulletsLeft = bulletsLeft;
    }
}
