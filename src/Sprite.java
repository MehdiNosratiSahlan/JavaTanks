import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;

/**
 * every object in game inherits from sprite class
 * all information required for objects are here
 */

public class Sprite implements Serializable{
    protected int x;
    protected int y;
    protected int width;
    protected int height;
    int type;
    double life = 0;
    protected BufferedImage image;
    boolean isVisible = true;

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }

    /**
     * object type
     */
    public static final int TANK = 0, BRICK = 1, ROUTE = 2, SOFT_BRICK = 3, IDIOT_ENEMY = 4,FOOD_UPGRADE = 5, TANK_ENEMY = 6, FIXED_ENEMY = 7, LAUNCHER = 8, FOOD_M = 9, FOOD_G = 11, FOOD_REPAIR = 12, MISSILE = 10, BULLET = 20,  FOOD_BRICK = 14, finish = 90;

    public Sprite(int x, int y, int type) {
        this.x = x;
        this.y = y;
        this.type = type;
        if (type == SOFT_BRICK)
            life = 3.99;
    }

    public int getType() {
        return type;
    }

    protected void getImageDimensions() {

        width = image.getWidth(null);
        height = image.getHeight(null);
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public Rectangle getBounds(BufferedImage img) {
        return new Rectangle(x, y, img.getWidth(), img.getHeight());
    }
}

