import java.io.*;
import java.lang.reflect.GenericArrayType;
import java.net.*;
import java.util.ArrayList;

/**
 *
 * which handles network connection and multiplayer mode and save load functions
 *
 * Created by Saeid Ghahremannejad on 7/13/2018.
 */
public class NetworkClass {
    public static boolean isServer = false;

    static String ip;
    public static ServerSocket serverSocket;
    public static Socket serverClient;
    public static ObjectOutputStream serverOut;
    public static ObjectInputStream serverIn;


    public static Socket clientSocket;
    public static ObjectOutputStream clientOut;
    public static ObjectInputStream clientIn;




    public static void saveGame(){
        GameObjects gameObjects = new GameObjects();

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream("src/sources/saved.jt");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            ObjectOutputStream oos = new ObjectOutputStream(fos);

            oos.writeObject(gameObjects);

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public static void loadGame(){

        FileInputStream fis = null;
        try {
            fis = new FileInputStream("src/sources/saved.jt");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            ObjectInputStream ois = new ObjectInputStream(fis);

            try {
                GameObjects gameObjects = (GameObjects) ois.readObject();


                CollideAssist.gameObjetcs = gameObjects.gObjects;
                GameState.GAME_BRICKS = gameObjects.bricks;
                GameState.tank = gameObjects.tank;
                GameState.tank.x = GameState.tank.x - GameState.xScroll + gameObjects.xScroll;
                GameState.tank.y = GameState.tank.y - GameState.yScroll + gameObjects.yScroll;
                GameState.GAME_SOFTBRICKS = gameObjects.softBricks;
                GameState.FOOD = gameObjects.food;
                GameState.IDIOT_ENEMY = gameObjects.idiotEnemies;
                GameState.finish = gameObjects.finish;
                GameState.LAUNCHER = gameObjects.launcher;
                GameState.FIXED_TANK = gameObjects.fixedTanks;
                GameState.TANK_ENEMY = gameObjects.tankEnemies;
                GameState.missiles = gameObjects.missiles;
                GameState.explosions = gameObjects.explosions;


            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }


        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    public static void setConnection(){
        if (isServer){





            try {
                serverSocket = new ServerSocket(7213);
                serverClient = serverSocket.accept();
                System.out.println("Server : client connected");
                serverIn = new ObjectInputStream(serverClient.getInputStream());
                serverOut = new ObjectOutputStream(serverClient.getOutputStream());

            } catch (IOException e) {
                e.printStackTrace();
            }




        }else{




            try {
                clientSocket = new Socket(ip,7213);
                clientOut = new ObjectOutputStream(clientSocket.getOutputStream()) ;
                clientIn = new ObjectInputStream(clientSocket.getInputStream()) ;
            } catch (IOException e) {
                e.printStackTrace();
            }



        }

    }
    public static void closeConnection(){
        if(isServer){
            try {
                serverIn.close();
                serverOut.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            try {
                clientIn.close();
                clientOut.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    public static void transfer(){
        if (isServer){





            send(serverOut);
            receive(serverIn);



        }else{

            receive(clientIn);
            send(clientOut);
        }
    }
    public static void send(ObjectOutputStream out){
        try {
            networkObjects gameObjects = new networkObjects();
            out.flush();
            out.reset();
            out.writeObject(gameObjects);

        } catch (IOException e) {
            e.printStackTrace();
            GameState.isMultiplayer = false;



        }
    }

    public static String myIP(){


        try(final DatagramSocket socket = new DatagramSocket()){
            try {
                socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
            ip = socket.getLocalAddress().getHostAddress();
            System.out.println(ip);

        } catch (SocketException e) {
            e.printStackTrace();
        }
        return ip;
    }



    public static void receive(ObjectInputStream in){
        try {

            networkObjects gameObjects = ((networkObjects)(in.readObject()));



            GameState.isFriendPaused = gameObjects.paused;
            GameState.friendTank = gameObjects.tank;
            GameState.friendTank.x = GameState.friendTank.x - GameState.xScroll + gameObjects.xScroll;
            GameState.friendTank.y = GameState.friendTank.y - GameState.yScroll + gameObjects.yScroll;

            ArrayList<Missile> missiles = new ArrayList(gameObjects.missiles);
            ArrayList<Missile> friendmissles = new ArrayList<>();
            ArrayList<Missile> myFriendmissles = new ArrayList<>();

            for (Missile m : missiles){

                m.x = m.x - GameState.xScroll + gameObjects.xScroll;
                m.y = m.y - GameState.yScroll + gameObjects.yScroll;
                m.xC = m.xC - GameState.xScroll + gameObjects.xScroll;
                m.yC = m.yC - GameState.yScroll + gameObjects.yScroll;

                if (!m.isEnemy  && ((isServer && m.isFriends)   ||  (!isServer && !m.isFriends) )){


                 friendmissles.add(m);

                }


            }
            for (Missile m : GameState.missiles){
                if (!m.isEnemy  && ((isServer && m.isFriends)   ||  (!isServer && !m.isFriends) )){


                    myFriendmissles.add(m);

                }


            }
            for (int i =myFriendmissles.size() ; i< friendmissles.size() ; i++){
                GameState.missiles.add(friendmissles.get(i));
            }


            if (!isServer) {
                ArrayList<Enemy> enemyTanks = new ArrayList(gameObjects.tankEnemies);
                for (int i = 0; i < GameState.TANK_ENEMY.size() ; i++){
                    GameState.TANK_ENEMY.get(i).targetX  = enemyTanks.get(i).targetX;
                    GameState.TANK_ENEMY.get(i).targetY  = enemyTanks.get(i).targetY;
                }
            }

            for (int i = 0; i < GameState.IDIOT_ENEMY.size() ; i++){

                if (GameState.IDIOT_ENEMY.get(i).getEnemyHitpoint() > gameObjects.idiotEnemies.get(i).getEnemyHitpoint()){
                    GameState.IDIOT_ENEMY.get(i).setEnemyHitpoint( gameObjects.idiotEnemies.get(i).getEnemyHitpoint());
                }
            }
            GameState.friendxScroll = gameObjects.xScroll;
            GameState.friendyScroll = gameObjects.yScroll;


        } catch (IOException e) {
            e.printStackTrace();
            GameState.isMultiplayer = false;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
}
