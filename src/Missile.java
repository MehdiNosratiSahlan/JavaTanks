/**
 * this class contains missiles information
 */


public class Missile extends Sprite{
    //private final int speed = 10;
    public static   int missileSpeed = 10;
    public  static int bulletSpeed = 15;
    public double angel;
    double x,y;
    double xC, yC;
    double xR , yR;
    public  boolean isEnemy;
    private boolean isVisible = true;
    public static int delayM = 300;
    public static int delayB = 100;
    int yInterval, xInterval;
    public boolean isFriends = false;

    public static double powerM = 0.5;
    public static double powerB = 0.2;

    public Missile(double x, double y,double angel, int type, double xc , double yc,  int xInterval , int yInterval, boolean isEnemy ,boolean isFriends ){
        super((int)x,(int)y,type);
        this.angel = angel;
        this.x = x;
        this.y = y;
        this.xC = xc;
        this.yC = yc;
        this.isEnemy = isEnemy;
        this.yInterval = yInterval;
        this.xInterval = xInterval;
        this.isFriends = isFriends;
    }
    public void shoot(){

        if (Tank.activeGun == Tank.MISSILE){
            if (!isEnemy)x += missileSpeed;
            else x += 10;
        }
        else{
            if (!isEnemy)x += bulletSpeed;
            else x += 15;
        }

        this.setX((int) x);
        this.setY((int) y);
        xR = (x-xC - xInterval) * Math.cos(angel) + (y-yC - yInterval) * Math.sin(angel) + xC + xInterval*Math.cos(angel + yInterval*Math.cos(angel)) + Math.cos(angel)*(GameFrame.imgGun.getWidth()/2 - GameFrame.imgMissile.getWidth()) + GameState.xScroll ;

        yR = (y-yC - yInterval) * Math.cos(angel) + (x-xC - xInterval) * Math.sin(angel) + yC + yInterval*Math.cos(angel) + xInterval*Math.sin(angel) + Math.sin(angel)*(GameFrame.imgGun.getWidth()/2 - GameFrame.imgMissile.getWidth()) + GameState.yScroll ;

    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }
}
