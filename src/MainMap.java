/*** In The Name of Allah ***/

import java.awt.EventQueue;
import javax.swing.JFrame;

/**
 * Program start.
 * 
 * @author Seyed Mohammad Ghaffarian
 */
public class MainMap {
	
    public static void main(String[] args) {
		// Initialize the global thread-pool
		ThreadPoolMap.init();
		
		// Show the game menu ...
		
		// After the player clicks 'PLAY' ...
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				GameFrameMap frame = new GameFrameMap("Map generator");
				frame.setLocationRelativeTo(null); // put frame at center of screen
				frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				frame.setVisible(true);
				frame.initBufferStrategy();
				// Create and execute the game-loop
				GameLoopMapGenerator game = new GameLoopMapGenerator(frame);
				game.init();
				ThreadPoolMap.execute(game);
				// and the game starts ...
			}
		});
    }
}
