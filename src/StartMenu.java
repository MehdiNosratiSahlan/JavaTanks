import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;

/**
 * Game starts here, the main menu
 */
public class StartMenu {
    //global variables
    private JFrame homeWindow;
    JPanel actorContainer;
    private JLabel[] actors;
    private boolean _easySelected = true, _mediumSelected = false, _hardSelected = false, _exitSelected = false, _multiSelected = false, settings = false;
    public static final int EASY = 0, MEDIUM = 1, HARD = 2;
    public static boolean soundOn = true;


    //Start Menu GUI constructor
    public StartMenu() throws IOException {

        //setup content pane and menu window
        homeWindow = new JFrame();
        homeWindow.setSize(new Dimension(1600, 700));
        homeWindow.setLocationRelativeTo(null);
        homeWindow.setUndecorated(true);
        JPanel mainPanel = new JPanel(new BorderLayout(5, 5));
        mainPanel.setBorder(new EmptyBorder(0, 50, 0, 0));
        homeWindow.setContentPane(mainPanel);

        //setup background image
        JPanel bg = new JPanel(new GridLayout(1, 1));
        bg.add(new JPanelWithBackground("src/sources/bg.jpg"));
        bg.setPreferredSize(new Dimension(1244, 700));
        mainPanel.add(bg, BorderLayout.CENTER);
        mainPanel.setBackground(new Color(51, 32, 1));

        //building the operators
        KeyEvents keyEventsHandler = new KeyEvents();
        actorContainer = new JPanel(new GridLayout(6, 1, 10, 10));
        actors = new JLabel[6];
        actors[0] = new JLabel("Play Easy          ");
        actors[1] = new JLabel("Play Medium    ");
        actors[2] = new JLabel("Play Hard         ");
        actors[5] = new JLabel("Quit Game         ");
        actors[3] = new JLabel("Multi player      ");
        actors[4] = new JLabel("Settings          ");

        for (int i = 0; i < 6; i++) {
            actorContainer.add(actors[i]);
            actors[i].setFont(new Font("Agency FB", Font.BOLD, 50));
        }
        actorContainer.addKeyListener(keyEventsHandler);
        actorContainer.setFocusable(true);
        actorContainer.setBackground(new Color(51, 32, 1));
        actorContainer.setPreferredSize(new Dimension(356, 700));
        ActorStateUpdate();


        //pack the home frame
        mainPanel.add(actorContainer, BorderLayout.WEST);

        //show home frame
        homeWindow.setVisible(true);
    }


    //update the selected actor
    public void ActorStateUpdate(){
        if (_easySelected) {
            for (int i = 0; i < 6; i++) {
                actors[i].setForeground(new Color(153, 99, 7));
                actors[i].setIcon(null);
            }
            actors[0].setForeground(Color.WHITE);
            actors[0].setIcon(new ImageIcon("src/sources/list.png"));
            actors[0].setHorizontalTextPosition(JLabel.LEFT);

        } else if (_mediumSelected) {
            for (int i = 0; i < 6; i++) {
                actors[i].setForeground(new Color(153, 99, 7));
                actors[i].setIcon(null);
            }
            actors[1].setForeground(Color.WHITE);
            actors[1].setIcon(new ImageIcon("src/sources/list.png"));
            actors[1].setHorizontalTextPosition(JLabel.LEFT);
        } else if (_hardSelected) {
            for (int i = 0; i < 6; i++) {
                actors[i].setForeground(new Color(153, 99, 7));
                actors[i].setIcon(null);
            }
            actors[2].setForeground(Color.WHITE);
            actors[2].setIcon(new ImageIcon("src/sources/list.png"));
            actors[2].setHorizontalTextPosition(JLabel.LEFT);
        } else if (_exitSelected) {
            for (int i = 0; i < 6; i++) {
                actors[i].setForeground(new Color(153, 99, 7));
                actors[i].setIcon(null);
            }
            actors[5].setForeground(Color.WHITE);
            actors[5].setIcon(new ImageIcon("src/sources/list.png"));
            actors[5].setHorizontalTextPosition(JLabel.LEFT);
        }
        else if (_multiSelected){
            for (int i = 0; i < 6; i++) {
                actors[i].setForeground(new Color(153, 99, 7));
                actors[i].setIcon(null);
            }
            actors[3].setForeground(Color.WHITE);
            actors[3].setIcon(new ImageIcon("src/sources/list.png"));
            actors[3].setHorizontalTextPosition(JLabel.LEFT);

        }
        else if (settings){
            for (int i = 0; i < 6; i++) {
                actors[i].setForeground(new Color(153, 99, 7));
                actors[i].setIcon(null);
            }
            actors[4].setForeground(Color.WHITE);
            actors[4].setIcon(new ImageIcon("src/sources/list.png"));
            actors[4].setHorizontalTextPosition(JLabel.LEFT);

        }
        actorContainer.updateUI();
    }

    //actors event handler class
    private class KeyEvents implements KeyListener {
        @Override
        public void keyPressed(KeyEvent e){
            if (e.getKeyCode() == KeyEvent.VK_UP) {
                if (_easySelected){
                    if (soundOn)Sound.playSound("src/sources/notallowed.wav");}
                else
                    if (soundOn)Sound.playSound("src/sources/menuMove.wav");


                    if (_mediumSelected) {
                        _mediumSelected = false;
                        _easySelected = true;
                        ActorStateUpdate();
                    }
                    if (_hardSelected) {
                        _hardSelected = false;
                        _mediumSelected = true;
                        ActorStateUpdate();
                    }
                    if (_multiSelected){
                        _multiSelected = false;
                        _hardSelected = true;
                        ActorStateUpdate();
                    }
                    if (settings){
                        settings = false;
                        _multiSelected = true;
                        ActorStateUpdate();
                    }
                    if (_exitSelected) {
                        _exitSelected = false;
                        settings = true;
                        ActorStateUpdate();
                    }

            } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                if (_exitSelected){
                    if (soundOn)Sound.playSound("src/sources/notallowed.wav");}
                else
                if (soundOn)Sound.playSound("src/sources/menuMove.wav");



                    if (_easySelected) {
                        _easySelected = false;
                        _mediumSelected = true;
                        ActorStateUpdate();
                    } else if (_mediumSelected) {
                        _mediumSelected = false;
                        _hardSelected = true;
                        ActorStateUpdate();
                    } else if (_hardSelected) {
                        _hardSelected = false;
                        _multiSelected = true;
                        ActorStateUpdate();
                    }
                    else if (_multiSelected){
                        _multiSelected = false;
                        settings = true;
                        ActorStateUpdate();
                    }
                    else if (settings){
                        settings = false;
                        _exitSelected = true;
                        ActorStateUpdate();
                    }

            } else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                if (soundOn) Sound.playSound("src/sources/select.wav");
                if (_easySelected) {
                    GameState.isMultiplayer = false;
                    MapUtils mapUtils = new MapUtils();
                    MapUtils.difficulty = 0;
                    Main.main(null);
                } else if (_mediumSelected) {
                    GameState.isMultiplayer = false;
                    MapUtils mapUtils = new MapUtils();
                    MapUtils.difficulty = 1;
                    Main.main(null);
                } else if (_hardSelected) {
                    GameState.isMultiplayer = false;
                    MapUtils mapUtils = new MapUtils();
                    MapUtils.difficulty = 2;
                    Main.main(null);
                } else if (_exitSelected) {
                    System.exit(0);
                }
                else if (settings){
                    Settings settings = new Settings();
                }
                else if (_multiSelected){
                    GameState.isMultiplayer = true;
                    int server = JOptionPane.showConfirmDialog(actorContainer, "are u server?");
                    if (server != 0 && server != 2){

                        String ip = JOptionPane.showInputDialog("friend IP: ", JOptionPane.QUESTION_MESSAGE);
                        if (ip != null){
                            NetworkClass.isServer = false;
                            NetworkClass.ip  = ip;
                            MapUtils mapUtils = new MapUtils();
                            MapUtils.difficulty = 0;
                            NetworkClass.setConnection();
                            Main.main(null);
                        }

                    }
                    else{
                        JOptionPane.showMessageDialog(actorContainer, "wait for your friend to connect..." + NetworkClass.myIP());
                        NetworkClass.isServer = true;
                        MapUtils.difficulty = 0;
                        MapUtils mapUtils = new MapUtils();
                        NetworkClass.setConnection();
                        Main.main(null);
                    }



                }
            }
        }

        @Override
        public void keyTyped(KeyEvent e) {

        }

        @Override
        public void keyReleased(KeyEvent e) {

        }
    }





    public static void main(String[] args) throws IOException {
        if (soundOn)Sound.playSound("src/sources/background.wav");

        StartMenu startMenu = new StartMenu();


    }
}
