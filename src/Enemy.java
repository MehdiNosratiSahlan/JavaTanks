/**
 * Enemy class contains all information about the enemy objects
 *
 * @author MnS
 */


public class Enemy extends Sprite {

    private int enemyType;
    private double enemyHitpoint;
    private int enemySpeed;
    /**
     * handle 4 types of enemies
     */
    public static final int TANK = 0, IDIOT = 1, LAUNCHER = 2, STICKY = 3;
    /**
     * enemy collisions
     */
    private boolean collideUP, collideDown, collideLeft, collideRight;

    public long moveTime, fireTime;
    /**
     * enemy aimm
     */
    int targetX, targetY;
    int firstX, firstY;
    /**
     * enemy rotations
     */
    double alpha;
    double gama, theta;
    boolean moveUp = false, moveDown = false, moveRight = false, moveLeft = false;


    public boolean isCollideUP() {
        return collideUP;
    }

    public void setCollideUP(boolean collideUP) {
        this.collideUP = collideUP;
    }

    public boolean isCollideDown() {
        return collideDown;
    }

    public void setCollideDown(boolean collideDown) {
        this.collideDown = collideDown;
    }

    public boolean isCollideLeft() {
        return collideLeft;
    }

    public void setCollideLeft(boolean collideLeft) {
        this.collideLeft = collideLeft;
    }

    public boolean isCollideRight() {
        return collideRight;
    }

    public void setCollideRight(boolean collideRight) {
        this.collideRight = collideRight;
    }


    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    private boolean isActive = false;

    /**
     * constructor for enemy class
     * @param x x of enemy
     * @param y y of enemy
     * @param enemyType  4 types of enemy detect
     */
    public Enemy(int x, int y, int enemyType) {
        super(x, y, 4);
        this.enemyType = enemyType;
        if ((enemyType == TANK || enemyType == IDIOT) && (MapUtils.difficulty == 0 || MapUtils.difficulty == 3)) {
            enemyHitpoint = 1.49;
        } else if (MapUtils.difficulty == 0 || MapUtils.difficulty == 3) {
            enemyHitpoint = 2.99;
        }

        if ((enemyType == TANK || enemyType == IDIOT) && MapUtils.difficulty == 1) {
            enemyHitpoint = 2.99;
        } else if (MapUtils.difficulty == 1) {
            enemyHitpoint = 3.99;
        }

        if ((enemyType == TANK || enemyType == IDIOT) && MapUtils.difficulty == 2) {
            enemyHitpoint = 3.99;
        } else if (MapUtils.difficulty == 2) {
            enemyHitpoint = 4.99;
        }


        firstX = this.getX();
        firstY = this.getY();

    }

    public int getEnemyType() {
        return enemyType;
    }

    public void setEnemyType(int enemyType) {
        this.enemyType = enemyType;
    }

    public double getEnemyHitpoint() {
        return enemyHitpoint;
    }

    public void setEnemyHitpoint(double enemyHitpoint) {
        this.enemyHitpoint = enemyHitpoint;
    }
}
