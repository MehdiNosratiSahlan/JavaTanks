/*** In The Name of Allah ***/


import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.*;

/**
 * The window on which the rendering is performed.
 * This example uses the modern BufferStrategy approach for double-buffering, 
 * actually it performs triple-buffering!
 * For more information on BufferStrategy check out:
 *    http://docs.oracle.com/javase/tutorial/extra/fullscreen/bufferstrategy.html
 *    http://docs.oracle.com/javase/8/docs/api/java/awt/image/BufferStrategy.html
 * 
 * @author Seyed Mohammad Ghaffarian
 */
public class GameFrameMap extends JFrame {
	
	public static final int GAME_HEIGHT = 1080;// 720p game resolution
	JButton save;
	public static final int GAME_WIDTH = 16 * GAME_HEIGHT / 9;  // wide aspect ratio
	//uncomment all /*...*/ in the class for using Tank icon instead of a simple circle

	public BufferedImage soil;
	public BufferedImage softWall;
	public BufferedImage ground;
	public BufferedImage tank;
	public BufferedImage tankEnemy;
	public BufferedImage launcher;
	public BufferedImage idiotEnemy;
	public BufferedImage fixedTank;
	public BufferedImage upgrader;
	public BufferedImage repair;
	public BufferedImage mashingunfood;
	public BufferedImage cannonfood;
	public BufferedImage finish;
	private long lastRender;
	private ArrayList<Float> fpsHistory;

	private BufferStrategy bufferStrategy;
	
	public GameFrameMap(String title) {
		super(title);
		setResizable(false);
		setSize(GAME_WIDTH, GAME_HEIGHT);
		lastRender = -1;
		fpsHistory = new ArrayList<>(100);



		try{
			soil = ImageIO.read(new File("src/sources/MapGen/soil.jpg"));
			ground = ImageIO.read(new File("src/sources/MapGen/ground.png"));
			softWall = ImageIO.read(new File("src/sources/MapGen/softWall1.png"));
			tank = ImageIO.read(new File("src/sources/MapGen/TANK.png"));
			tankEnemy = ImageIO.read(new File("src/sources/MapGen/EnemyTank.png"));
			launcher = ImageIO.read(new File("src/sources/MapGen/launcher.png"));
			idiotEnemy = ImageIO.read(new File("src/sources/MapGen/idiotEnemy.png"));
			fixedTank = ImageIO.read(new File("src/sources/MapGen/fixedTank.png"));

			upgrader = ImageIO.read(new File("src/sources/MapGen/upgrader.png"));
			repair = ImageIO.read(new File("src/sources/MapGen/repair.png"));
			mashingunfood = ImageIO.read(new File("src/sources/MapGen/mashingunfood.png"));
			cannonfood = ImageIO.read(new File("src/sources/MapGen/cannonfood.png"));
			finish = ImageIO.read(new File("src/sources/MapGen/finish.png"));
		}
		catch(IOException e){
			System.out.println(e);
		}
	}
	
	/**
	 * This must be called once after the JFrame is shown:
	 *    frame.setVisible(true);
	 * and before any rendering is started.
	 */
	public void initBufferStrategy() {
		// Triple-buffering
		createBufferStrategy(3);
		bufferStrategy = getBufferStrategy();
	}

	
	/**
	 * Game rendering with triple-buffering using BufferStrategy.
	 */
	public void render(GameStateMap state) {
		// Render single frame
		do {
			// The following loop ensures that the contents of the drawing buffer
			// are consistent in case the underlying surface was recreated
			do {
				// Get a new graphics context every time through the loop
				// to make sure the strategy is validated
				Graphics2D graphics = (Graphics2D) bufferStrategy.getDrawGraphics();
				try {
					doRendering(graphics, state);
				} finally {
					// Dispose the graphics
					graphics.dispose();
				}
				// Repeat the rendering if the drawing buffer contents were restored
			} while (bufferStrategy.contentsRestored());

			// Display the buffer
			bufferStrategy.show();
			// Tell the system to do the drawing NOW;
			// otherwise it can take a few extra ms and will feel jerky!
			Toolkit.getDefaultToolkit().sync();

		// Repeat the rendering if the drawing buffer was lost
		} while (bufferStrategy.contentsLost());
	}
	
	/**
	 * Rendering all game elements based on the game state.
	 */
	private void doRendering(Graphics2D g2d, GameStateMap state) {

		// Draw background
		g2d.translate(GameStateMap.xScroll,GameStateMap.yScroll);
		g2d.setColor(Color.GRAY);
		g2d.fillRect(0, 0, GAME_WIDTH, GAME_HEIGHT);
		// Draw ball
		g2d.setColor(Color.BLACK);

		//	g2d.fillOval(state.locX, state.locY, state.diam, state.diam);

//		g2d.drawImage(image,state.locX,state.locY,null);*/
		for (int i =0 ; i <state.y ; i++){
				for (int j=0 ; j<state.x ; j++) {
					int index = i * state.x + j;
					char ch = state.map.get(index);
					int x = j;
					int y = i;
					//System.out.println(i + "  " + x + "  " + y + "   ");
					if (ch == 'O') {
						g2d.drawImage(ground, (x) * 100 +150, (y) * 100 +100, null);
					}
					if (ch == 'S') {
						g2d.drawImage(soil, (x ) * 100 +150, (y) * 100 +100, null);
					}
					if (ch == 'b') {
						g2d.drawImage(softWall, (x ) * 100 +150, (y) * 100 +100, null);
					}

					if (ch == 'T') {
						g2d.drawImage(tank, (x) * 100 +150, (y) * 100 +100, null);
					}
					if (ch == 'i') {
						g2d.drawImage(idiotEnemy, (x ) * 100 +150, (y) * 100 +100, null);
					}
					if (ch == 'e') {
						g2d.drawImage(tankEnemy, (x ) * 100 +150, (y) * 100 +100, null);
					}

					if (ch == 'L') {
						g2d.drawImage(launcher, (x) * 100 +150, (y) * 100 +100, null);
					}
					if (ch == 'l') {
						g2d.drawImage(fixedTank, (x ) * 100 +150, (y) * 100 +100, null);
					}
					if (ch == 'u') {
						g2d.drawImage(repair, (x ) * 100 +150, (y) * 100 +100, null);
					}

					if (ch == 'U') {
						g2d.drawImage(upgrader, (x) * 100 +150, (y) * 100 +100, null);
					}
					if (ch == 'r') {
						g2d.drawImage(mashingunfood, (x ) * 100 +150, (y) * 100 +100, null);
					}
					if (ch == 'R') {
						g2d.drawImage(cannonfood, (x ) * 100 +150, (y) * 100 +100, null);
					}
					if (ch == 'F') {
						g2d.drawImage(finish, (x ) * 100 +150, (y) * 100 +100, null);
					}

				}
		}
		if (state.mouseX>=150 && state.mouseX<= 150 + state.x *100 && state.mouseY>=100 && state.mouseY<= 100 + state.y *100 ) {
			if (GameStateMap.ch == 'S')
				g2d.drawImage(soil, state.mouseX - GameStateMap.xScroll, state.mouseY - GameStateMap.yScroll, 40, 40, null);
			if (GameStateMap.ch == 'O')
				g2d.drawImage(ground, state.mouseX - GameStateMap.xScroll, state.mouseY - GameStateMap.yScroll, 40, 40, null);
			if (GameStateMap.ch == 'b')
				g2d.drawImage(softWall, state.mouseX - GameStateMap.xScroll, state.mouseY - GameStateMap.yScroll, 40, 40, null);

			if (GameStateMap.ch == 'T')
				g2d.drawImage(tank, state.mouseX - GameStateMap.xScroll, state.mouseY - GameStateMap.yScroll, 40, 40, null);
			if (GameStateMap.ch == 'i')
				g2d.drawImage(idiotEnemy, state.mouseX - GameStateMap.xScroll, state.mouseY - GameStateMap.yScroll, 40, 40, null);
			if (GameStateMap.ch == 'e')
				g2d.drawImage(tankEnemy, state.mouseX - GameStateMap.xScroll, state.mouseY - GameStateMap.yScroll, 40, 40, null);


			if (GameStateMap.ch == 'L')
				g2d.drawImage(launcher, state.mouseX - GameStateMap.xScroll, state.mouseY - GameStateMap.yScroll, 40, 40, null);
			if (GameStateMap.ch == 'l')
				g2d.drawImage(fixedTank, state.mouseX - GameStateMap.xScroll, state.mouseY - GameStateMap.yScroll, 40, 40, null);
			if (GameStateMap.ch == 'u')
				g2d.drawImage(repair, state.mouseX - GameStateMap.xScroll, state.mouseY - GameStateMap.yScroll, 40, 40, null);


			if (GameStateMap.ch == 'U')
				g2d.drawImage(upgrader, state.mouseX - GameStateMap.xScroll, state.mouseY - GameStateMap.yScroll, 40, 40, null);
			if (GameStateMap.ch == 'r')
				g2d.drawImage(mashingunfood, state.mouseX - GameStateMap.xScroll, state.mouseY - GameStateMap.yScroll, 40, 40, null);
			if (GameStateMap.ch == 'R')
				g2d.drawImage(cannonfood, state.mouseX - GameStateMap.xScroll, state.mouseY - GameStateMap.yScroll, 40, 40, null);
			if (GameStateMap.ch == 'F')
				g2d.drawImage(finish, state.mouseX - GameStateMap.xScroll, state.mouseY - GameStateMap.yScroll, 40, 40, null);


		}







		g2d.setColor(new Color(220, 217, 165));
		g2d.fillRect(0 - GameStateMap.xScroll,100 - GameStateMap.yScroll,150,40000);
		g2d.fillRect(0 - GameStateMap.xScroll,0 - GameStateMap.yScroll,40000,100);
		g2d.drawImage(soil,25 - GameStateMap.xScroll,110 - GameStateMap.yScroll,40,40,null);
		g2d.drawImage(ground,85- GameStateMap.xScroll,110- GameStateMap.yScroll,40,40,null);
		g2d.drawImage(softWall,25- GameStateMap.xScroll,170- GameStateMap.yScroll,40,40,null);
		g2d.drawImage(tank,85- GameStateMap.xScroll,170- GameStateMap.yScroll,40,40,null);
		g2d.drawImage(tankEnemy,25- GameStateMap.xScroll,230- GameStateMap.yScroll,40,40,null);
		g2d.drawImage(launcher,85- GameStateMap.xScroll,230- GameStateMap.yScroll,40,40,null);
		g2d.drawImage(idiotEnemy,25- GameStateMap.xScroll,290- GameStateMap.yScroll,40,40,null);
		g2d.drawImage(fixedTank,85- GameStateMap.xScroll,290- GameStateMap.yScroll,40,40,null);

		g2d.drawImage(repair,25- GameStateMap.xScroll,350- GameStateMap.yScroll,40,40,null);
		g2d.drawImage(upgrader,85- GameStateMap.xScroll,350- GameStateMap.yScroll,40,40,null);
		g2d.drawImage(mashingunfood,25- GameStateMap.xScroll,410- GameStateMap.yScroll,40,40,null);
		g2d.drawImage(cannonfood,85- GameStateMap.xScroll,410- GameStateMap.yScroll,40,40,null);


		g2d.drawImage(finish,25- GameStateMap.xScroll,470- GameStateMap.yScroll,40,40,null);
		// Print FPS info


		g2d.setColor(Color.BLACK);
		g2d.setFont(new Font("Sans Serif", Font.BOLD, 28));
		g2d.drawString("Press \"CTRL + S\" to save.",25 - GameStateMap.xScroll,80  - GameStateMap.yScroll);

	}
	
}
