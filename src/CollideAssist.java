import java.awt.*;
import java.util.ArrayList;

/**
 * gets the array list of map file and adds the objects to relevant lists
 */
public class CollideAssist {
    public static ArrayList<Sprite> gameObjetcs = new ArrayList<Sprite>();

    public CollideAssist() {
        System.out.println(MapUtils.map.size());
        for (int i = 0; i < MapUtils.map.size(); i++) {
            for (int j = 0; j < MapUtils.map.get(i).length(); j++) {
                switch (MapUtils.map.get(i).charAt(j)) {
                    case 'S':
                        gameObjetcs.add(new Sprite(j * 100, i * 100, Sprite.BRICK));
                        GameState.GAME_BRICKS.add(new Sprite(j*100, i*100, Sprite.BRICK));
                        break;
                    case 'O':
                        gameObjetcs.add(new Sprite(j * 100, i * 100, Sprite.ROUTE));
                        break;
                    case 'T':
                        gameObjetcs.add(new Sprite(j * 100, i * 100, Sprite.TANK));
                        GameState.tank.x = j*100;
                        GameState.tank.y = i * 100;
                        break;

                    case 'b':
                        gameObjetcs.add(new Sprite(j * 100, i * 100, Sprite.SOFT_BRICK));
                        GameState.GAME_SOFTBRICKS.add(new Sprite(j * 100, i * 100, Sprite.SOFT_BRICK));
                        break;
                    case 'B':
                        gameObjetcs.add(new Sprite(j*100, i*100, Sprite.FOOD_BRICK));
                        GameState.GAME_SOFTBRICKS.add(new Sprite(j*100, i*100, Sprite.SOFT_BRICK));
                        if (Math.random() > 0.5){
                            gameObjetcs.add(new Sprite(j*100, i*100, Sprite.FOOD_UPGRADE));
                            GameState.FOOD.add(new Sprite(j*100,i*100, Sprite.FOOD_UPGRADE));
                        }
                        else{
                            gameObjetcs.add(new Sprite(j*100, i*100, Sprite.FOOD_REPAIR));
                            GameState.FOOD.add(new Sprite(j*100,i*100, Sprite.FOOD_REPAIR));
                        }
                        GameState.FOOD.add(new Sprite(j*100, i*100, Sprite.FOOD_BRICK));
                        break;
                    case 'i':
                        gameObjetcs.add(new Sprite(j*100, i*100, Sprite.IDIOT_ENEMY));
                        GameState.IDIOT_ENEMY.add(new Enemy(j*100, i*100, Enemy.IDIOT));
                        break;
                    case 'e':
                        gameObjetcs.add(new Sprite(j*100, i*100, Sprite.TANK_ENEMY));
                        GameState.TANK_ENEMY.add(new Enemy(j*100, i*100, Enemy.TANK));
                        break;
                    case 'l':
                        gameObjetcs.add(new Sprite(j*100, i * 100, Sprite.FIXED_ENEMY));
                        GameState.FIXED_TANK.add(new Enemy(j*100, i*100, Enemy.STICKY));
                        break;
                    case 'L':
                        gameObjetcs.add(new Sprite(j*100, i * 100, Sprite.LAUNCHER));
                        GameState.LAUNCHER.add(new Enemy(j*100, i*100, Enemy.LAUNCHER));
                    case 'r':
                        gameObjetcs.add(new Sprite(j*100,i*100, Sprite.FOOD_G));
                        GameState.FOOD.add(new Sprite(j*100,i*100, Sprite.FOOD_G));
                        break;
                    case 'R':
                        gameObjetcs.add(new Sprite(j*100,i*100, Sprite.FOOD_M));
                        GameState.FOOD.add(new Sprite(j*100,i*100, Sprite.FOOD_M));
                        break;
                    case 'U':
                        gameObjetcs.add(new Sprite(j*100,i*100, Sprite.FOOD_UPGRADE));
                        GameState.FOOD.add(new Sprite(j*100,i*100, Sprite.FOOD_UPGRADE));
                        break;
                    case 'u':
                        gameObjetcs.add(new Sprite(j*100,i*100, Sprite.FOOD_REPAIR));
                        GameState.FOOD.add(new Sprite(j*100,i*100, Sprite.FOOD_REPAIR));
                        break;
                    case 'F':
                        gameObjetcs.add(new Sprite(j*100,i*100, Sprite.finish));
                        GameState.finish = new Sprite(j*100, i*100, Sprite.finish);
                        break;



                }
            }
        }
    }

}