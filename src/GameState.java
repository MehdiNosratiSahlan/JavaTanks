/*** In The Name of Allah ***/

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Random;

/**
 * This class holds the state of game and all of its elements.
 * This class also handles user inputs, which affect the game state.
 *
 * @author Seyed Mohammad Ghaffarian
 * @author MnS     this class handles whole logic of the game
 */
public class GameState {
    public static boolean isMultiplayer = false;
    public static boolean isFriendPaused = false;
    public static long contime = System.currentTimeMillis();
    public static ArrayList<Sprite> GAME_BRICKS = new ArrayList<Sprite>();
    public static ArrayList<Sprite> GAME_SOFTBRICKS = new ArrayList<Sprite>();
    public static Tank tank = new Tank();
    public static Tank friendTank = new Tank();
    public static ArrayList<Enemy> IDIOT_ENEMY = new ArrayList<Enemy>();
    public static ArrayList<Enemy> TANK_ENEMY = new ArrayList<Enemy>();
    public static ArrayList<Enemy> FIXED_TANK = new ArrayList<Enemy>();
    public static ArrayList<Enemy> LAUNCHER = new ArrayList<Enemy>();
    public static ArrayList<Sprite> FOOD = new ArrayList<Sprite>();
    public static int friendsMissilesSize;
    public static Sprite finish;
    public boolean isFinished = false;
    public boolean gameOver;
    public boolean loose = false;
    public static boolean isPaused = false;
    public static int yScroll = 0;
    public static int xScroll = 0;
    public static int friendyScroll = 0;
    public static int friendxScroll = 0;
    private boolean keyUP, keyDOWN, keyRIGHT, keyLEFT;
    private boolean mousePress;
    public static int mouseX, mouseY;
    public static int MISSILE_ACTIVE = 10, BULLETS_ACTIVE = 20;
    private KeyHandler keyHandler;
    private MouseHandler mouseHandler;
    public static ArrayList<Missile> missiles = new ArrayList<Missile>();
    long looseTime;
    int looseTimec = 0;
    long time = System.currentTimeMillis();
    private String cheat = "";
    long chtTime = System.currentTimeMillis();
    long fireTime = System.currentTimeMillis();
    public static int fired = 0;
    public static boolean firedAnim = false;
    public static ArrayList<Explosion> explosions = new ArrayList<>();
    ArrayList<Rectangle> launcherRec = new ArrayList<Rectangle>();
    ArrayList<Rectangle> fixedTankRec = new ArrayList<Rectangle>();
    ArrayList<Rectangle> foodRec = new ArrayList<Rectangle>();
    Random random = new Random();


    /**
     * constructor for game state class
     */
    public GameState() {

        //  tank.x = 1200;
        //  tank.y = 6600;
        gameOver = false;
        loose = false;
        //
        keyUP = false;
        keyDOWN = false;
        keyRIGHT = false;
        keyLEFT = false;
        //
        mousePress = false;
        mouseX = 0;
        mouseY = 0;
        //
        keyHandler = new KeyHandler();
        mouseHandler = new MouseHandler();
    }

    /**
     * The method which updates the game state.
     * which is executed with every frame which gameFrame renders it
     */
    public void update() {

        /**
         * this part handles tank rotation
         */
        try {
            double deltaY = (this.mouseY - this.tank.y - GameFrame.imgTnk.getHeight() / 2);
            double deltaX = (this.mouseX - this.tank.x - GameFrame.imgTnk.getWidth() / 2);
            double tan = deltaY / deltaX;
            tank.alpha = Math.atan(tan);
            if (this.mouseX < this.tank.x + GameFrame.imgTnk.getWidth() / 2)
                tank.alpha = Math.PI + tank.alpha;
        } catch (Exception e) {
            tank.alpha = Math.PI / 2;
            if (this.mouseY < this.tank.y + GameFrame.imgTnk.getHeight() / 2)
                tank.alpha = -tank.alpha;
        }
        if (keyUP && !keyDOWN) {
            tank.gama = -Math.PI / 2;
            if (keyRIGHT)
                tank.gama += Math.PI / 4;
            if (keyLEFT)
                tank.gama -= Math.PI / 4;
        }
        if (keyDOWN && !keyUP) {
            tank.gama = Math.PI / 2;
            if (keyRIGHT)
                tank.gama -= Math.PI / 4;
            if (keyLEFT)
                tank.gama += Math.PI / 4;
        }
        if (keyRIGHT && !keyLEFT) {
            tank.gama = 0;
            if (keyUP)
                tank.gama -= Math.PI / 4;
            if (keyDOWN)
                tank.gama += Math.PI / 4;
        }
        if (keyLEFT && !keyRIGHT) {
            tank.gama = Math.PI;
            if (keyUP)
                tank.gama += Math.PI / 4;
            if (keyDOWN)
                tank.gama -= Math.PI / 4;
        }
        if (tank.gama - tank.theta > Math.PI / 2) {
            tank.gama -= Math.PI;
        }
        if (tank.theta - tank.gama > Math.PI / 2) {
            tank.gama += Math.PI;
        }


        /**
         * add some rectangles to handle the collisions
         */
        Rectangle tankRec = new Rectangle();
        Rectangle friendTankRec = new Rectangle();
        ArrayList<Rectangle> bricks = new ArrayList<Rectangle>();
        ArrayList<Rectangle> softBricksRec = new ArrayList<Rectangle>();
        ArrayList<Rectangle> idiotEnemiesRec = new ArrayList<Rectangle>();
        ArrayList<Rectangle> tankEnemiesRec = new ArrayList<Rectangle>();
        for (Sprite s : CollideAssist.gameObjetcs
                ) {
            if (s.getType() == Sprite.BRICK) {
                bricks.add(s.getBounds(GameFrame.soil));
            } else if (s.getType() == Sprite.TANK) {
                tankRec = tank.getBounds(GameFrame.imgTnk);
                if (isMultiplayer)
                    friendTankRec = friendTank.getBounds(GameFrame.imgTnk);
            }

        }

        /**
         * to create the explosion effect
         */
        if (System.currentTimeMillis() - fireTime > 50) {
            fired = 0;
        }
        if (System.currentTimeMillis() - fireTime > 10) {
            firedAnim = false;
        }

        /**
         * to open fire when clicked. + set a delay to avoid continous fire
         */
        if (mousePress) {
            //   missiles.add(new Missile(tankRec.x + tankRec.width / 2 - GameFrame.imgMissile.getWidth() / 2 + GameFrame.imgGun.getWidth() / 2, tankRec.y + tankRec.getHeight() / 2 - GameFrame.imgMissile.getHeight() / 2, tank.alpha, MISSILE_ACTIVE)); //////////////
            if (System.currentTimeMillis() - time > Missile.delayM && tank.getActiveGun() == Tank.MISSILE) {
                Sound.playSound("src/sources/shoot.wav");
                if (tank.getMissilesLeft() > 0) {
                    if (NetworkClass.isServer) {
                        missiles.add(new Missile(tankRec.x + tankRec.width / 2 - GameFrame.imgMissile.getWidth() / 2 + GameFrame.imgGun.getWidth() / 2, tankRec.y + tankRec.getHeight() / 2 - GameFrame.imgMissile.getHeight() / 2, tank.alpha, MISSILE_ACTIVE, tankRec.x + tankRec.width / 2, tankRec.y + tankRec.getHeight() / 2, 00, 0, false, false)); //////////////

                    } else {
                        missiles.add(new Missile(tankRec.x + tankRec.width / 2 - GameFrame.imgMissile.getWidth() / 2 + GameFrame.imgGun.getWidth() / 2, tankRec.y + tankRec.getHeight() / 2 - GameFrame.imgMissile.getHeight() / 2, tank.alpha, MISSILE_ACTIVE, tankRec.x + tankRec.width / 2, tankRec.y + tankRec.getHeight() / 2, 00, 0, false, true)); //////////////

                    }
                    tank.setMissilesLeft(tank.getMissilesLeft() - 1);
                }

                time = System.currentTimeMillis();
                fired = 1;
                firedAnim = true;
                fireTime = System.currentTimeMillis();
            }
            if (System.currentTimeMillis() - time > Missile.delayB && tank.getActiveGun() == Tank.MACHINE_GUN) {
                Sound.playSound("src/sources/shootB.wav");
                if (tank.getBulletsLeft() > 0) {
                    if (NetworkClass.isServer) {
                        missiles.add(new Missile(tankRec.x + tankRec.width / 2 - GameFrame.imgMissile.getWidth() / 2 + GameFrame.imgGun.getWidth() / 2, tankRec.y + tankRec.getHeight() / 2 - GameFrame.imgMissile.getHeight() / 2, tank.alpha, BULLETS_ACTIVE
                                , tankRec.x + tankRec.width / 2, tankRec.y + tankRec.getHeight() / 2, 0, 0, false, false)); //////////////
                    } else {
                        missiles.add(new Missile(tankRec.x + tankRec.width / 2 - GameFrame.imgMissile.getWidth() / 2 + GameFrame.imgGun.getWidth() / 2, tankRec.y + tankRec.getHeight() / 2 - GameFrame.imgMissile.getHeight() / 2, tank.alpha, BULLETS_ACTIVE
                                , tankRec.x + tankRec.width / 2, tankRec.y + tankRec.getHeight() / 2, 0, 0, false, true)); //////////////

                    }

                    tank.setBulletsLeft(tank.getBulletsLeft() - 1);
                }
                time = System.currentTimeMillis();
                fired = 1;
                firedAnim = true;
                fireTime = System.currentTimeMillis();
            }


        }


        /**
         * to shoot the added missiles
         */
        for (Missile ms : missiles
                ) {
            if (ms.isVisible()) {
                ms.shoot();


            }


        }

        /**
         * to detect collision between missiles & bricks
         */
        for (Missile m : missiles
                ) {
            for (Rectangle b : bricks
                    ) {

                if (m.xR >= b.getX() && m.xR <= b.getX() + b.getWidth() && m.yR >= b.getY() && m.yR <= b.getY() + b.getHeight()) { /////////////////////////////////////////////////////////
                    explosions.add(new Explosion((int) m.xR, (int) m.yR, m.type));
                    m.setVisible(false);
                    m.xR = -2000;
                    m.yR = -2000;
                    m.xC = -42000;

                    // m.speed = 0;
                }
            }
        }


        /**
         * add softbrick rectangles
         */
        for (Sprite s : GAME_SOFTBRICKS
                ) {
            if (s.getType() == Sprite.SOFT_BRICK) {
                softBricksRec.add(s.getBounds(GameFrame.softBrick1));

            }
        }

        fixedTankRec.clear();
        launcherRec.clear();

        /**
         * add enemy rectangles
         */
        for (Enemy e : IDIOT_ENEMY) {
            idiotEnemiesRec.add(e.getBounds(GameFrame.enemy));
        }
        for (Enemy e : TANK_ENEMY
                ) {
            tankEnemiesRec.add(e.getBounds(GameFrame.enemyTank));
        }
        for (Enemy e : FIXED_TANK) {
            fixedTankRec.add(e.getBounds(GameFrame.enemyFixedTank));
        }
        for (Enemy e : LAUNCHER) {
            launcherRec.add(e.getBounds(GameFrame.launcher));
        }


        /**
         * add food recs
         */
        for (Sprite s : FOOD
                ) {
            foodRec.add(s.getBounds(GameFrame.upgradeFood));
        }


        /**
         * collision of missiles and soft bricks
         */
        Rectangle t = new Rectangle();
        for (Missile m : missiles
                ) {
            for (Sprite s : GAME_SOFTBRICKS
                    ) {
                t = s.getBounds(GameFrame.softBrick1);

                if (m.xR >= t.getX() && m.xR <= t.getX() + t.getWidth() && m.yR >= t.getY() && m.yR <= t.getY() + t.getHeight()) { /////////////////////////////////////////////////////////
                    explosions.add(new Explosion((int) m.xR, (int) m.yR, m.type));
                    m.setVisible(false);
                    m.x = -2000;
                    m.y = -2000;
                    m.xR = -2000;
                    m.yR = -2000;
                    m.xC = -42000;


                    //System.out.println("DD");
                    if (!m.isEnemy) {
                        if (m.getType() == MISSILE_ACTIVE)
                            s.life -= Missile.powerM;
                        else
                            s.life -= Missile.powerB;
                    }
                    if ((int) s.life == 0) {
                        softBricksRec.remove(t);
                        s.setX(-200);
                        s.setY(-200);
                        s.setVisible(false);
                        softBricksRec.add(t);
                    }


                    //softBricks.remove(s);
                    // m.speed = 0;

                }

            }
        }

        /**
         * collision of missiles and  idiot enemies
         */
        for (Missile m : missiles
                ) {
            for (Enemy s : IDIOT_ENEMY
                    ) {
                if (s.getType() == Sprite.IDIOT_ENEMY) {
                    t = s.getBounds(GameFrame.enemy);

                    if (m.xR >= t.getX() && m.xR <= t.getX() + t.getWidth() && m.yR >= t.getY() && m.yR <= t.getY() + t.getHeight()) { /////////////////////////////////////////////////////////
                        if (!m.isEnemy) {
                            explosions.add(new Explosion((int) m.xR, (int) m.yR, m.type));
                            m.setVisible(false);
                            m.x = -2000;
                            m.y = -2000;
                            m.xR = -2000;
                            m.yR = -2000;
                            m.xC = -42000;


                            if (m.getType() == MISSILE_ACTIVE)
                                s.setEnemyHitpoint(s.getEnemyHitpoint() - Missile.powerM);
                            else
                                s.setEnemyHitpoint(s.getEnemyHitpoint() - Missile.powerB);
                            if ((int) s.getEnemyHitpoint() == 0) {

                                idiotEnemiesRec.remove(t);
                                s.setVisible(false);
                                s.setX(-200);
                                s.setY(-200);
                                idiotEnemiesRec.add(t);
                            }
                        }


                        //softBricks.remove(s);
                        // m.speed = 0;

                    }

                }
            }

        }


        /**
         * collision of missiles and enemy tanks
         */
        for (Missile m : missiles
                ) {
            for (Enemy s : TANK_ENEMY
                    ) {

                t = s.getBounds(GameFrame.enemyTank);

                if (m.xR >= t.getX() && m.xR <= t.getX() + t.getWidth() && m.yR >= t.getY() && m.yR <= t.getY() + t.getHeight()) { /////////////////////////////////////////////////////////
                    if (!m.isEnemy) {
                        explosions.add(new Explosion((int) m.xR, (int) m.yR, m.type));
                        m.setVisible(false);
                        m.x = -2000;
                        m.y = -2000;
                        m.xR = -2000;
                        m.yR = -2000;
                        m.xC = -42000;


                        if (m.getType() == MISSILE_ACTIVE)
                            s.setEnemyHitpoint(s.getEnemyHitpoint() - Missile.powerM);
                        else
                            s.setEnemyHitpoint(s.getEnemyHitpoint() - Missile.powerB);
                        if ((int) s.getEnemyHitpoint() == 0) {

                            //  enemies.remove(t);
                            s.setVisible(false);
                            s.setX(-200);
                            s.setY(-200);
                            //   enemies.add(t);
                        }
                    }


                    //softBricks.remove(s);
                    // m.speed = 0;

                }


            }

        }

        /**
         * collision of missiles and enemy fixed tanks
         */
        for (Missile m : missiles
                ) {
            for (Enemy s : FIXED_TANK
                    ) {

                t = s.getBounds(GameFrame.enemyFixedTank);

                if (m.xR >= t.getX() && m.xR <= t.getX() + t.getWidth() && m.yR >= t.getY() && m.yR <= t.getY() + t.getHeight()) { /////////////////////////////////////////////////////////
                    if (!m.isEnemy) {
                        explosions.add(new Explosion((int) m.xR, (int) m.yR, m.type));
                        m.setVisible(false);
                        m.x = -2000;
                        m.y = -2000;
                        m.xR = -2000;
                        m.yR = -2000;
                        m.xC = -42000;


                        if (m.getType() == MISSILE_ACTIVE)
                            s.setEnemyHitpoint(s.getEnemyHitpoint() - Missile.powerM);
                        else
                            s.setEnemyHitpoint(s.getEnemyHitpoint() - Missile.powerB);
                        if ((int) s.getEnemyHitpoint() == 0) {

                            //  enemies.remove(t);
                            s.setVisible(false);
                            s.setX(-200);
                            s.setY(-200);
                        }


                    }
                    //softBricks.remove(s);
                    // m.speed = 0;

                }


            }

        }

        /**
         * collision of missiles and launchers
         */
        for (Missile m : missiles
                ) {

            for (Enemy s : LAUNCHER
                    ) {

                t = s.getBounds(GameFrame.launcher);

                if (m.xR >= t.getX() && m.xR <= t.getX() + t.getWidth() && m.yR >= t.getY() && m.yR <= t.getY() + t.getHeight()) { /////////////////////////////////////////////////////////
                    if (!m.isEnemy) {
                        explosions.add(new Explosion((int) m.xR, (int) m.yR, m.type));

                        m.setVisible(false);
                        m.x = -2000;
                        m.y = -2000;
                        m.xR = -2000;
                        m.yR = -2000;
                        m.xC = -42000;

                        if (m.getType() == MISSILE_ACTIVE)
                            s.setEnemyHitpoint(s.getEnemyHitpoint() - Missile.powerM);
                        else
                            s.setEnemyHitpoint(s.getEnemyHitpoint() - Missile.powerB);


                        if ((int) s.getEnemyHitpoint() == 0) {

                            //  enemies.remove(t);
                            s.setVisible(false);
                            s.setX(-200);
                            s.setY(-200);
                            //    launcherRec.add(t);
                        }
                    }


                    //softBricks.remove(s);
                    // m.speed = 0;

                }


            }

        }


        /**
         *
         *
         * to set active the idiot enemies
         *
         *
         *
         *
         *
         */
        for (Enemy e : IDIOT_ENEMY
                ) {
            if (isMultiplayer) {
                if ((Math.abs(e.getX() - tankRec.x - xScroll) <= 300 && Math.abs(e.getY() - tankRec.y - yScroll) <= 300) || (Math.abs(e.getX() - friendTankRec.x - xScroll) <= 300 && Math.abs(e.getY() - friendTankRec.y - yScroll) <= 300)) {
                    e.setActive(true);
                    e.setVisible(true);
                } else {
                    e.setActive(false);
                    e.setVisible(false);
                }
            } else {
                if ((Math.abs(e.getX() - tankRec.x - xScroll) <= 300 && Math.abs(e.getY() - tankRec.y - yScroll) <= 300)) {
                    e.setActive(true);
                    e.setVisible(true);
                } else {
                    e.setActive(false);
                    e.setVisible(false);
                }
            }
        }

        /**
         * for detecting collision of idiot enemies and other objects
         */
        double upX = 0;
        double upY = 0;
        double downX = 0;
        double downY = 0;
        double leftX = 0;
        double leftY = 0;
        double rightX = 0;
        double rightY = 0;
        boolean interRight = false, interLeft = false, interUp = false, interDown = false;
        //check enemy collision with walls
        for (int i = 0; i < IDIOT_ENEMY.size(); i++) {

            interRight = false;
            interLeft = false;
            interUp = false;
            interDown = false;
            IDIOT_ENEMY.get(i).setCollideRight(false);
            IDIOT_ENEMY.get(i).setCollideUP(false);
            IDIOT_ENEMY.get(i).setCollideDown(false);
            IDIOT_ENEMY.get(i).setCollideLeft(false);

            upX = idiotEnemiesRec.get(i).x + idiotEnemiesRec.get(i).getWidth() / 2;
            upY = idiotEnemiesRec.get(i).y;
            downX = idiotEnemiesRec.get(i).x + idiotEnemiesRec.get(i).getWidth() / 2;
            downY = idiotEnemiesRec.get(i).y + idiotEnemiesRec.get(i).getHeight();
            leftX = idiotEnemiesRec.get(i).x;
            leftY = idiotEnemiesRec.get(i).y + idiotEnemiesRec.get(i).getHeight() / 2;
            rightX = idiotEnemiesRec.get(i).x + idiotEnemiesRec.get(i).getWidth();
            rightY = idiotEnemiesRec.get(i).y + idiotEnemiesRec.get(i).getHeight() / 2;


            for (Rectangle r : idiotEnemiesRec
                    ) {
                if (!((upY >= r.getY() && upY <= r.getY() + r.getHeight() && upX >= r.getX() && upX <= r.getX() + r.getWidth()) && (downY >= r.getY() && downY <= r.getY() + r.getHeight() && downX >= r.getX() && downX <= r.getX() + r.getWidth()))) {

                    if (upY >= r.getY() && upY <= r.getY() + r.getHeight() && upX >= r.getX() && upX <= r.getX() + r.getWidth()) {
                        interUp = true;
                        IDIOT_ENEMY.get(i).setCollideUP(true);


                    }
                    if (downY >= r.getY() && downY <= r.getY() + r.getHeight() && downX >= r.getX() && downX <= r.getX() + r.getWidth()) {
                        interDown = true;
                        IDIOT_ENEMY.get(i).setCollideDown(true);
                    }
                    if (leftY >= r.getY() && leftY <= r.getY() + r.getHeight() && leftX >= r.getX() && leftX <= r.getX() + r.getWidth()) {
                        interLeft = true;
                        IDIOT_ENEMY.get(i).setCollideLeft(true);

                    }
                    if (rightY >= r.getY() && rightY <= r.getY() + r.getHeight() && rightX >= r.getX() && rightX <= r.getX() + r.getWidth()) {
                        interRight = true;
                        IDIOT_ENEMY.get(i).setCollideRight(true);

                    }
                }
            }


            for (Rectangle r : launcherRec
                    ) {

                if (upY >= r.getY() && upY <= r.getY() + r.getHeight() && upX >= r.getX() && upX <= r.getX() + r.getWidth()) {
                    interUp = true;
                    IDIOT_ENEMY.get(i).setCollideUP(true);


                }
                if (downY >= r.getY() && downY <= r.getY() + r.getHeight() && downX >= r.getX() && downX <= r.getX() + r.getWidth()) {
                    interDown = true;
                    IDIOT_ENEMY.get(i).setCollideDown(true);
                }
                if (leftY >= r.getY() && leftY <= r.getY() + r.getHeight() && leftX >= r.getX() && leftX <= r.getX() + r.getWidth()) {
                    interLeft = true;
                    IDIOT_ENEMY.get(i).setCollideLeft(true);

                }
                if (rightY >= r.getY() && rightY <= r.getY() + r.getHeight() && rightX >= r.getX() && rightX <= r.getX() + r.getWidth()) {
                    interRight = true;
                    IDIOT_ENEMY.get(i).setCollideRight(true);

                }
            }

            for (Rectangle r : tankEnemiesRec
                    ) {

                if (upY >= r.getY() && upY <= r.getY() + r.getHeight() && upX >= r.getX() && upX <= r.getX() + r.getWidth()) {
                    interUp = true;
                    IDIOT_ENEMY.get(i).setCollideUP(true);


                }
                if (downY >= r.getY() && downY <= r.getY() + r.getHeight() && downX >= r.getX() && downX <= r.getX() + r.getWidth()) {
                    interDown = true;
                    IDIOT_ENEMY.get(i).setCollideDown(true);
                }
                if (leftY >= r.getY() && leftY <= r.getY() + r.getHeight() && leftX >= r.getX() && leftX <= r.getX() + r.getWidth()) {
                    interLeft = true;
                    IDIOT_ENEMY.get(i).setCollideLeft(true);

                }
                if (rightY >= r.getY() && rightY <= r.getY() + r.getHeight() && rightX >= r.getX() && rightX <= r.getX() + r.getWidth()) {
                    interRight = true;
                    IDIOT_ENEMY.get(i).setCollideRight(true);

                }
            }
            for (Rectangle r : fixedTankRec
                    ) {

                if (upY >= r.getY() && upY <= r.getY() + r.getHeight() && upX >= r.getX() && upX <= r.getX() + r.getWidth()) {
                    interUp = true;
                    IDIOT_ENEMY.get(i).setCollideUP(true);


                }
                if (downY >= r.getY() && downY <= r.getY() + r.getHeight() && downX >= r.getX() && downX <= r.getX() + r.getWidth()) {
                    interDown = true;
                    IDIOT_ENEMY.get(i).setCollideDown(true);
                }
                if (leftY >= r.getY() && leftY <= r.getY() + r.getHeight() && leftX >= r.getX() && leftX <= r.getX() + r.getWidth()) {
                    interLeft = true;
                    IDIOT_ENEMY.get(i).setCollideLeft(true);

                }
                if (rightY >= r.getY() && rightY <= r.getY() + r.getHeight() && rightX >= r.getX() && rightX <= r.getX() + r.getWidth()) {
                    interRight = true;
                    IDIOT_ENEMY.get(i).setCollideRight(true);

                }
            }

            for (Rectangle r : bricks
                    ) {

                if (upY >= r.getY() && upY <= r.getY() + r.getHeight() && upX >= r.getX() && upX <= r.getX() + r.getWidth()) {
                    interUp = true;
                    IDIOT_ENEMY.get(i).setCollideUP(true);


                }
                if (downY >= r.getY() && downY <= r.getY() + r.getHeight() && downX >= r.getX() && downX <= r.getX() + r.getWidth()) {
                    interDown = true;
                    IDIOT_ENEMY.get(i).setCollideDown(true);
                }
                if (leftY >= r.getY() && leftY <= r.getY() + r.getHeight() && leftX >= r.getX() && leftX <= r.getX() + r.getWidth()) {
                    interLeft = true;
                    IDIOT_ENEMY.get(i).setCollideLeft(true);

                }
                if (rightY >= r.getY() && rightY <= r.getY() + r.getHeight() && rightX >= r.getX() && rightX <= r.getX() + r.getWidth()) {
                    interRight = true;
                    IDIOT_ENEMY.get(i).setCollideRight(true);

                }
            }
            for (Rectangle r : softBricksRec
                    ) {

                if (upY >= r.getY() && upY <= r.getY() + r.getHeight() && upX >= r.getX() && upX <= r.getX() + r.getWidth()) {
                    interUp = true;
                    IDIOT_ENEMY.get(i).setCollideUP(true);


                }
                if (downY >= r.getY() && downY <= r.getY() + r.getHeight() && downX >= r.getX() && downX <= r.getX() + r.getWidth()) {
                    interDown = true;
                    IDIOT_ENEMY.get(i).setCollideDown(true);
                }
                if (leftY >= r.getY() && leftY <= r.getY() + r.getHeight() && leftX >= r.getX() && leftX <= r.getX() + r.getWidth()) {
                    interLeft = true;
                    IDIOT_ENEMY.get(i).setCollideLeft(true);

                }
                if (rightY >= r.getY() && rightY <= r.getY() + r.getHeight() && rightX >= r.getX() && rightX <= r.getX() + r.getWidth()) {
                    interRight = true;
                    IDIOT_ENEMY.get(i).setCollideRight(true);

                }
            }
            boolean dead = false;


            Rectangle r = tankRec;

            /**
             *  to handle the game scrol effects
             */
            r.setBounds((int) (r.getX() + xScroll), (int) (r.getY() + yScroll), (int) r.getWidth(), (int) r.getHeight());
            if (upY >= r.getY() && upY <= r.getY() + r.getHeight() && upX >= r.getX() && upX <= r.getX() + r.getWidth()) {

                IDIOT_ENEMY.get(i).setEnemyHitpoint(0);
                dead = true;

                tank.setHitPoint(tank.getHitPoint() - 1);

                explosions.add(new Explosion((int) upX, (int) upY, MISSILE_ACTIVE));

            }
            if (downY >= r.getY() && downY <= r.getY() + r.getHeight() && downX >= r.getX() && downX <= r.getX() + r.getWidth()) {
                dead = true;
                tank.setHitPoint(tank.getHitPoint() - 1);
                explosions.add(new Explosion((int) downX, (int) downY, MISSILE_ACTIVE));
            }
            if (leftY >= r.getY() && leftY <= r.getY() + r.getHeight() && leftX >= r.getX() && leftX <= r.getX() + r.getWidth()) {
                dead = true;
                tank.setHitPoint(tank.getHitPoint() - 1);
                explosions.add(new Explosion((int) leftX, (int) leftY, MISSILE_ACTIVE));

            }
            if (rightY >= r.getY() && rightY <= r.getY() + r.getHeight() && rightX >= r.getX() && rightX <= r.getX() + r.getWidth()) {
                dead = true;
                tank.setHitPoint(tank.getHitPoint() - 1);
                explosions.add(new Explosion((int) rightX, (int) rightY, MISSILE_ACTIVE));
            }
            r.setBounds((int) (r.getX() - xScroll), (int) (r.getY() - yScroll), (int) r.getWidth(), (int) r.getHeight());

            if (isMultiplayer) {
                r = friendTankRec;


                r.setBounds((int) (r.getX() + xScroll), (int) (r.getY() + yScroll), (int) r.getWidth(), (int) r.getHeight());
                if (upY >= r.getY() && upY <= r.getY() + r.getHeight() && upX >= r.getX() && upX <= r.getX() + r.getWidth()) {

                    IDIOT_ENEMY.get(i).setEnemyHitpoint(0);
                    dead = true;

                    friendTank.setHitPoint(tank.getHitPoint() - 1);

                    explosions.add(new Explosion((int) upX, (int) upY, MISSILE_ACTIVE));

                }
                if (downY >= r.getY() && downY <= r.getY() + r.getHeight() && downX >= r.getX() && downX <= r.getX() + r.getWidth()) {
                    dead = true;
                    friendTank.setHitPoint(tank.getHitPoint() - 1);
                    explosions.add(new Explosion((int) downX, (int) downY, MISSILE_ACTIVE));
                }
                if (leftY >= r.getY() && leftY <= r.getY() + r.getHeight() && leftX >= r.getX() && leftX <= r.getX() + r.getWidth()) {
                    dead = true;
                    friendTank.setHitPoint(tank.getHitPoint() - 1);
                    explosions.add(new Explosion((int) leftX, (int) leftY, MISSILE_ACTIVE));

                }
                if (rightY >= r.getY() && rightY <= r.getY() + r.getHeight() && rightX >= r.getX() && rightX <= r.getX() + r.getWidth()) {
                    dead = true;
                    friendTank.setHitPoint(tank.getHitPoint() - 1);
                    explosions.add(new Explosion((int) rightX, (int) rightY, MISSILE_ACTIVE));
                }
                r.setBounds((int) (r.getX() - xScroll), (int) (r.getY() - yScroll), (int) r.getWidth(), (int) r.getHeight());
            }

            /**
             * check idiot enemy death
             */
            if (dead) {

                idiotEnemiesRec.remove(t);
                IDIOT_ENEMY.get(i).setVisible(false);
                IDIOT_ENEMY.get(i).setX(-200);
                IDIOT_ENEMY.get(i).setY(-200);
                idiotEnemiesRec.add(t);
            }

            /**
             * move idiot enemy
             */
            if (IDIOT_ENEMY.get(i).isActive()) {

                if ((Math.pow((tankRec.x - IDIOT_ENEMY.get(i).getX() + xScroll), 2) + Math.pow((tankRec.y - IDIOT_ENEMY.get(i).getY() + yScroll), 2) <= Math.pow((friendTankRec.x - IDIOT_ENEMY.get(i).getX() + xScroll), 2) + Math.pow((friendTankRec.y - IDIOT_ENEMY.get(i).getY() + yScroll), 2)) || !isMultiplayer) {
                    if (IDIOT_ENEMY.get(i).getX() >= tankRec.x + xScroll && !IDIOT_ENEMY.get(i).isCollideLeft()) {
                        IDIOT_ENEMY.get(i).setX(IDIOT_ENEMY.get(i).getX() - 5);

                    } else if (!IDIOT_ENEMY.get(i).isCollideRight() && IDIOT_ENEMY.get(i).getX() <= tankRec.x + xScroll) {
                        IDIOT_ENEMY.get(i).setX(IDIOT_ENEMY.get(i).getX() + 5);
                    }
                    if (IDIOT_ENEMY.get(i).getY() >= tankRec.y + yScroll && !IDIOT_ENEMY.get(i).isCollideUP()) {
                        IDIOT_ENEMY.get(i).setY(IDIOT_ENEMY.get(i).getY() - 5);
                    } else if (!IDIOT_ENEMY.get(i).isCollideDown() && IDIOT_ENEMY.get(i).getY() <= tankRec.y + yScroll) {
                        IDIOT_ENEMY.get(i).setY(IDIOT_ENEMY.get(i).getY() + 5);
                    }
                } else {
                    if (IDIOT_ENEMY.get(i).getX() >= friendTankRec.x + xScroll && !IDIOT_ENEMY.get(i).isCollideLeft()) {
                        IDIOT_ENEMY.get(i).setX(IDIOT_ENEMY.get(i).getX() - 5);

                    } else if (!IDIOT_ENEMY.get(i).isCollideRight() && IDIOT_ENEMY.get(i).getX() <= friendTankRec.x + xScroll) {
                        IDIOT_ENEMY.get(i).setX(IDIOT_ENEMY.get(i).getX() + 5);
                    }
                    if (IDIOT_ENEMY.get(i).getY() >= friendTankRec.y + yScroll && !IDIOT_ENEMY.get(i).isCollideUP()) {
                        IDIOT_ENEMY.get(i).setY(IDIOT_ENEMY.get(i).getY() - 5);
                    } else if (!IDIOT_ENEMY.get(i).isCollideDown() && IDIOT_ENEMY.get(i).getY() <= friendTankRec.y + yScroll) {
                        IDIOT_ENEMY.get(i).setY(IDIOT_ENEMY.get(i).getY() + 5);
                    }
                }
            }


        }

        /**
         * set active launcher
         */

        for (Enemy e : LAUNCHER
                ) {
            if (((Math.abs(e.getX() - tankRec.x - xScroll) <= 500 && Math.abs(e.getY() - tankRec.y - yScroll) <= 500)) || ((Math.abs(e.getX() - friendTankRec.x - xScroll) <= 500 && Math.abs(e.getY() - friendTankRec.y - yScroll) <= 500 && isMultiplayer))) {
                e.setActive(true);
            } else {
                e.setActive(false);
            }
        }


        /**
         * to rotate the launcher gun
         */
        for (int i = 0; i < LAUNCHER.size(); i++) {
            double deltaY;
            double deltaX;
            double mdeltaY = (tank.getY() + GameState.yScroll - LAUNCHER.get(i).getY() - GameFrame.imgTnk.getHeight() / 2 + GameFrame.launcher.getHeight() / 2);
            double mdeltaX = (tank.getX() + GameState.xScroll - LAUNCHER.get(i).getX() - GameFrame.imgTnk.getWidth() / 2 + GameFrame.launcher.getWidth() / 2);
            double fdeltaY = (friendTank.getY() + GameState.yScroll - LAUNCHER.get(i).getY() - GameFrame.imgTnk.getHeight() / 2 + GameFrame.launcher.getHeight() / 2);
            double fdeltaX = (friendTank.getX() + GameState.xScroll - LAUNCHER.get(i).getX() - GameFrame.imgTnk.getWidth() / 2 + GameFrame.launcher.getWidth() / 2);

            if (Math.pow(mdeltaX, 2) + Math.pow(mdeltaY, 2) <= Math.pow(fdeltaX, 2) + Math.pow(fdeltaY, 2) || !isMultiplayer) {
                deltaY = mdeltaY;
                deltaX = mdeltaX;
            } else {
                deltaY = fdeltaY;
                deltaX = fdeltaX;
            }

            try {
                double tan = deltaY / deltaX;
                LAUNCHER.get(i).alpha = Math.atan(tan);
                if (deltaX < 0)
                    LAUNCHER.get(i).alpha = Math.PI + LAUNCHER.get(i).alpha;
            } catch (Exception e) {
                LAUNCHER.get(i).alpha = Math.PI / 2;
                if (deltaY < 0)
                    LAUNCHER.get(i).alpha = -LAUNCHER.get(i).alpha;
            }


            /**
             * launcher opens fire
             */
            if (LAUNCHER.get(i).isActive()) {
                if (LAUNCHER.get(i).fireTime + 1200 < System.currentTimeMillis()) {
                    missiles.add(new Missile(LAUNCHER.get(i).x + GameFrame.launcher.getWidth() / 2 + GameFrame.launcherGun.getWidth() / 2 - xScroll
                            , LAUNCHER.get(i).y + GameFrame.launcher.getHeight() / 3 - GameFrame.imgMissile.getHeight() / 2 - yScroll
                            , LAUNCHER.get(i).alpha, MISSILE_ACTIVE,
                            LAUNCHER.get(i).x + GameFrame.launcher.getWidth() / 2 - xScroll,
                            LAUNCHER.get(i).y + GameFrame.launcher.getHeight() / 2 - yScroll, 0, -GameFrame.launcher.getHeight() / 6, true, false)); //////////////


                    missiles.add(new Missile(LAUNCHER.get(i).x + GameFrame.launcher.getWidth() / 2 + GameFrame.launcherGun.getWidth() / 2 - xScroll
                            , LAUNCHER.get(i).y + GameFrame.launcher.getHeight() * 2 / 3 - GameFrame.imgMissile.getHeight() / 2 - yScroll
                            , LAUNCHER.get(i).alpha, MISSILE_ACTIVE,
                            LAUNCHER.get(i).x + GameFrame.launcher.getWidth() / 2 - xScroll,
                            LAUNCHER.get(i).y + GameFrame.launcher.getHeight() / 2 - yScroll, 0, +GameFrame.launcher.getHeight() / 6, true, false)); //////////////


                    LAUNCHER.get(i).fireTime = System.currentTimeMillis();
                }
            }


        }


        /**
         * to set active enemy fixed tank
         */
        for (Enemy e : FIXED_TANK
                ) {
            if (((Math.abs(e.getX() - tankRec.x - xScroll) <= 500 && Math.abs(e.getY() - tankRec.y - yScroll) <= 500)) || ((Math.abs(e.getX() - friendTankRec.x - xScroll) <= 500 && Math.abs(e.getY() - friendTankRec.y - yScroll) <= 500) && isMultiplayer)) {
                e.setActive(true);
            } else {
                e.setActive(false);
            }
        }


        /**
         * fixed tank rotate gun
         */
        for (int i = 0; i < FIXED_TANK.size(); i++) {
            double deltaY;
            double deltaX;
            double mdeltaY = (tank.getY() + GameState.yScroll - FIXED_TANK.get(i).getY() - GameFrame.imgTnk.getHeight() / 2 + GameFrame.enemyFixedTank.getHeight() / 2);
            double mdeltaX = (tank.getX() + GameState.xScroll - FIXED_TANK.get(i).getX() - GameFrame.imgTnk.getWidth() / 2 + GameFrame.enemyFixedTank.getWidth() / 2);
            double fdeltaY = (friendTank.getY() + GameState.yScroll - FIXED_TANK.get(i).getY() - GameFrame.imgTnk.getHeight() / 2 + GameFrame.enemyFixedTank.getHeight() / 2);
            double fdeltaX = (friendTank.getX() + GameState.xScroll - FIXED_TANK.get(i).getX() - GameFrame.imgTnk.getWidth() / 2 + GameFrame.enemyFixedTank.getWidth() / 2);

            if (Math.pow(mdeltaX, 2) + Math.pow(mdeltaY, 2) <= Math.pow(fdeltaX, 2) + Math.pow(fdeltaY, 2) || !isMultiplayer) {
                deltaX = mdeltaX;
                deltaY = mdeltaY;
            } else {
                deltaY = fdeltaY;
                deltaX = fdeltaX;

            }
            try {
                double tan = deltaY / deltaX;
                FIXED_TANK.get(i).alpha = Math.atan(tan);
                if (deltaX < 0)
                    FIXED_TANK.get(i).alpha = Math.PI + FIXED_TANK.get(i).alpha;
            } catch (Exception e) {
                FIXED_TANK.get(i).alpha = Math.PI / 2;
                if (deltaY < 0)
                    FIXED_TANK.get(i).alpha = -FIXED_TANK.get(i).alpha;
            }

            /**
             * fixed tank open fire
             */
            if (FIXED_TANK.get(i).isActive()) {
                if (FIXED_TANK.get(i).fireTime + 1000 < System.currentTimeMillis()) {
                    missiles.add(new Missile(FIXED_TANK.get(i).x + GameFrame.enemyFixedTank.getWidth() / 2 + GameFrame.enemyFixedTankGun.getWidth() / 2 - xScroll
                            , FIXED_TANK.get(i).y + GameFrame.enemyFixedTank.getHeight() / 2 - GameFrame.imgMissile.getHeight() / 2 - yScroll
                            , FIXED_TANK.get(i).alpha, MISSILE_ACTIVE,
                            FIXED_TANK.get(i).x + GameFrame.enemyFixedTank.getWidth() / 2 - xScroll,
                            FIXED_TANK.get(i).y + GameFrame.enemyFixedTank.getHeight() / 2 - yScroll, 0, 0, true, false)); //////////////


                    FIXED_TANK.get(i).fireTime = System.currentTimeMillis();
                }
            }


        }

        /**
         * to effect the tank hitted by a bullet or missile
         */
        for (Missile ms : missiles
                ) {
            if (ms.isEnemy) {

                if (ms.getType() == MISSILE_ACTIVE) {
                    if (ms.xR - xScroll >= tankRec.getX() && ms.xR - xScroll <= tankRec.getX() + tankRec.getWidth() && ms.yR - yScroll >= tankRec.getY() && ms.yR - yScroll <= tankRec.getY() + tankRec.getHeight()) {
                        explosions.add(new Explosion((int) ms.xR, (int) ms.yR, MISSILE_ACTIVE));
                        ms.setVisible(false);
                        ms.x = -2000;
                        ms.y = -2000;
                        ms.xR = -2000;
                        ms.yR = -2000;
                        ms.xC = -42000;

                        tank.setHitPoint(tank.getHitPoint() - 0.005);

                    }
                }
                if (ms.getType() == BULLETS_ACTIVE) {
                    if (ms.xR - xScroll >= tankRec.getX() && ms.xR - xScroll <= tankRec.getX() + tankRec.getWidth() && ms.yR - yScroll >= tankRec.getY() && ms.yR - yScroll <= tankRec.getY() + tankRec.getHeight()) {
                        explosions.add(new Explosion((int) ms.xR, (int) ms.yR, MISSILE_ACTIVE));
                        ms.setVisible(false);
                        ms.x = -2000;
                        ms.y = -2000;
                        ms.xR = -2000;
                        ms.yR = -2000;
                        ms.xC = -42000;

                        tank.setHitPoint(tank.getHitPoint() - 0.01);

                    }


                }


                if (isMultiplayer) {
                    if (ms.getType() == MISSILE_ACTIVE) {
                        if (ms.xR - xScroll >= friendTankRec.getX() && ms.xR - xScroll <= friendTankRec.getX() + friendTankRec.getWidth() && ms.yR - yScroll >= friendTankRec.getY() && ms.yR - yScroll <= friendTankRec.getY() + friendTankRec.getHeight()) {
                            explosions.add(new Explosion((int) ms.xR, (int) ms.yR, MISSILE_ACTIVE));
                            ms.setVisible(false);
                            ms.x = -2000;
                            ms.y = -2000;
                            ms.xR = -2000;
                            ms.yR = -2000;
                            ms.xC = -42000;


                        }
                    }
                    if (ms.getType() == BULLETS_ACTIVE) {
                        if (ms.xR - xScroll >= friendTankRec.getX() && ms.xR - xScroll <= friendTankRec.getX() + friendTankRec.getWidth() && ms.yR - yScroll >= friendTankRec.getY() && ms.yR - yScroll <= friendTankRec.getY() + friendTankRec.getHeight()) {
                            explosions.add(new Explosion((int) ms.xR, (int) ms.yR, MISSILE_ACTIVE));
                            ms.setVisible(false);
                            ms.x = -2000;
                            ms.y = -2000;
                            ms.xR = -2000;
                            ms.yR = -2000;
                            ms.xC = -42000;


                        }
                    }
                }

            }

        }


        /**
         * to set active the tank enemies
         */
        for (Enemy e : TANK_ENEMY
                ) {
            if ((Math.abs(e.getX() - tankRec.x - xScroll) <= 500 && Math.abs(e.getY() - tankRec.y - yScroll) <= 500) || ((Math.abs(e.getX() - friendTankRec.x - xScroll) <= 500 && Math.abs(e.getY() - friendTankRec.y - yScroll) <= 500) && isMultiplayer)) {
                e.setActive(true);
            } else {
                e.setActive(false);
            }
        }

        /**
         * to detect the collision of enemy tank and bricks
         */
        for (int i = 0; i < TANK_ENEMY.size(); i++) {


            double deltaY;
            double deltaX;
            double mdelatY = (tank.getY() + GameState.yScroll - TANK_ENEMY.get(i).getY() - GameFrame.imgTnk.getHeight() / 2 + GameFrame.enemyTank.getHeight() / 2);
            double mdeltaX = (tank.getX() + GameState.xScroll - TANK_ENEMY.get(i).getX() - GameFrame.imgTnk.getWidth() / 2 + GameFrame.enemyTank.getWidth() / 2);
            double fdelatY = (friendTank.getY() + GameState.yScroll - TANK_ENEMY.get(i).getY() - GameFrame.imgTnk.getHeight() / 2 + GameFrame.enemyTank.getHeight() / 2);
            double fdeltaX = (friendTank.getX() + GameState.xScroll - TANK_ENEMY.get(i).getX() - GameFrame.imgTnk.getWidth() / 2 + GameFrame.enemyTank.getWidth() / 2);

            if (Math.pow(mdelatY, 2) + Math.pow(mdeltaX, 2) <= Math.pow(fdelatY, 2) + Math.pow(fdeltaX, 2) || !isMultiplayer) {
                deltaY = mdelatY;
                deltaX = mdeltaX;
            } else {
                deltaY = fdelatY;
                deltaX = fdeltaX;
            }
            try {
                double tan = deltaY / deltaX;
                TANK_ENEMY.get(i).alpha = Math.atan(tan);
                if (deltaX < 0)
                    TANK_ENEMY.get(i).alpha = Math.PI + TANK_ENEMY.get(i).alpha;
            } catch (Exception e) {
                TANK_ENEMY.get(i).alpha = Math.PI / 2;
                if (deltaY < 0)
                    TANK_ENEMY.get(i).alpha = -TANK_ENEMY.get(i).alpha;
            }
            interRight = false;
            interLeft = false;
            interUp = false;
            interDown = false;
            TANK_ENEMY.get(i).setCollideRight(false);
            TANK_ENEMY.get(i).setCollideUP(false);
            TANK_ENEMY.get(i).setCollideDown(false);
            TANK_ENEMY.get(i).setCollideLeft(false);

            upX = tankEnemiesRec.get(i).x + tankEnemiesRec.get(i).getWidth() / 2;
            upY = tankEnemiesRec.get(i).y;
            downX = tankEnemiesRec.get(i).x + tankEnemiesRec.get(i).getWidth() / 2;
            downY = tankEnemiesRec.get(i).y + tankEnemiesRec.get(i).getHeight();
            leftX = tankEnemiesRec.get(i).x;
            leftY = tankEnemiesRec.get(i).y + tankEnemiesRec.get(i).getHeight() / 2;
            rightX = tankEnemiesRec.get(i).x + tankEnemiesRec.get(i).getWidth();
            rightY = tankEnemiesRec.get(i).y + tankEnemiesRec.get(i).getHeight() / 2;


            for (Rectangle r : bricks
                    ) {

                if (upY >= r.getY() && upY <= r.getY() + r.getHeight() && upX >= r.getX() && upX <= r.getX() + r.getWidth()) {
                    interUp = true;
                    TANK_ENEMY.get(i).setCollideUP(true);


                }
                if (downY >= r.getY() && downY <= r.getY() + r.getHeight() && downX >= r.getX() && downX <= r.getX() + r.getWidth()) {
                    interDown = true;
                    TANK_ENEMY.get(i).setCollideDown(true);
                }
                if (leftY >= r.getY() && leftY <= r.getY() + r.getHeight() && leftX >= r.getX() && leftX <= r.getX() + r.getWidth()) {
                    interLeft = true;
                    TANK_ENEMY.get(i).setCollideLeft(true);

                }
                if (rightY >= r.getY() && rightY <= r.getY() + r.getHeight() && rightX >= r.getX() && rightX <= r.getX() + r.getWidth()) {
                    interRight = true;
                    TANK_ENEMY.get(i).setCollideRight(true);

                }
            }
            for (Rectangle r : softBricksRec
                    ) {

                if (upY >= r.getY() && upY <= r.getY() + r.getHeight() && upX >= r.getX() && upX <= r.getX() + r.getWidth()) {
                    interUp = true;
                    TANK_ENEMY.get(i).setCollideUP(true);


                }
                if (downY >= r.getY() && downY <= r.getY() + r.getHeight() && downX >= r.getX() && downX <= r.getX() + r.getWidth()) {
                    interDown = true;
                    TANK_ENEMY.get(i).setCollideDown(true);
                }
                if (leftY >= r.getY() && leftY <= r.getY() + r.getHeight() && leftX >= r.getX() && leftX <= r.getX() + r.getWidth()) {
                    interLeft = true;
                    TANK_ENEMY.get(i).setCollideLeft(true);

                }
                if (rightY >= r.getY() && rightY <= r.getY() + r.getHeight() && rightX >= r.getX() && rightX <= r.getX() + r.getWidth()) {
                    interRight = true;
                    TANK_ENEMY.get(i).setCollideRight(true);

                }
            }

            for (Rectangle r : launcherRec
                    ) {

                if (upY >= r.getY() && upY <= r.getY() + r.getHeight() && upX >= r.getX() && upX <= r.getX() + r.getWidth()) {
                    interUp = true;
                    TANK_ENEMY.get(i).setCollideUP(true);


                }
                if (downY >= r.getY() && downY <= r.getY() + r.getHeight() && downX >= r.getX() && downX <= r.getX() + r.getWidth()) {
                    interDown = true;
                    TANK_ENEMY.get(i).setCollideDown(true);
                }
                if (leftY >= r.getY() && leftY <= r.getY() + r.getHeight() && leftX >= r.getX() && leftX <= r.getX() + r.getWidth()) {
                    interLeft = true;
                    TANK_ENEMY.get(i).setCollideLeft(true);

                }
                if (rightY >= r.getY() && rightY <= r.getY() + r.getHeight() && rightX >= r.getX() && rightX <= r.getX() + r.getWidth()) {
                    interRight = true;
                    TANK_ENEMY.get(i).setCollideRight(true);

                }
            }


            for (Rectangle r : fixedTankRec
                    ) {

                if (upY >= r.getY() && upY <= r.getY() + r.getHeight() && upX >= r.getX() && upX <= r.getX() + r.getWidth()) {
                    interUp = true;
                    TANK_ENEMY.get(i).setCollideUP(true);


                }
                if (downY >= r.getY() && downY <= r.getY() + r.getHeight() && downX >= r.getX() && downX <= r.getX() + r.getWidth()) {
                    interDown = true;
                    TANK_ENEMY.get(i).setCollideDown(true);
                }
                if (leftY >= r.getY() && leftY <= r.getY() + r.getHeight() && leftX >= r.getX() && leftX <= r.getX() + r.getWidth()) {
                    interLeft = true;
                    TANK_ENEMY.get(i).setCollideLeft(true);

                }
                if (rightY >= r.getY() && rightY <= r.getY() + r.getHeight() && rightX >= r.getX() && rightX <= r.getX() + r.getWidth()) {
                    interRight = true;
                    TANK_ENEMY.get(i).setCollideRight(true);

                }
            }


            for (Rectangle r : idiotEnemiesRec
                    ) {

                if (upY >= r.getY() && upY <= r.getY() + r.getHeight() && upX >= r.getX() && upX <= r.getX() + r.getWidth()) {
                    interUp = true;
                    TANK_ENEMY.get(i).setCollideUP(true);


                }
                if (downY >= r.getY() && downY <= r.getY() + r.getHeight() && downX >= r.getX() && downX <= r.getX() + r.getWidth()) {
                    interDown = true;
                    TANK_ENEMY.get(i).setCollideDown(true);
                }
                if (leftY >= r.getY() && leftY <= r.getY() + r.getHeight() && leftX >= r.getX() && leftX <= r.getX() + r.getWidth()) {
                    interLeft = true;
                    TANK_ENEMY.get(i).setCollideLeft(true);

                }
                if (rightY >= r.getY() && rightY <= r.getY() + r.getHeight() && rightX >= r.getX() && rightX <= r.getX() + r.getWidth()) {
                    interRight = true;
                    TANK_ENEMY.get(i).setCollideRight(true);

                }
            }


            for (Rectangle r : tankEnemiesRec

                    ) {
                if (!((upY >= r.getY() && upY <= r.getY() + r.getHeight() && upX >= r.getX() && upX <= r.getX() + r.getWidth()) && (downY >= r.getY() && downY <= r.getY() + r.getHeight() && downX >= r.getX() && downX <= r.getX() + r.getWidth()))) {
                    if (upY >= r.getY() && upY <= r.getY() + r.getHeight() && upX >= r.getX() && upX <= r.getX() + r.getWidth()) {
                        interUp = true;
                        TANK_ENEMY.get(i).setCollideUP(true);


                    }
                    if (downY >= r.getY() && downY <= r.getY() + r.getHeight() && downX >= r.getX() && downX <= r.getX() + r.getWidth()) {
                        interDown = true;
                        TANK_ENEMY.get(i).setCollideDown(true);
                    }
                    if (leftY >= r.getY() && leftY <= r.getY() + r.getHeight() && leftX >= r.getX() && leftX <= r.getX() + r.getWidth()) {
                        interLeft = true;
                        TANK_ENEMY.get(i).setCollideLeft(true);

                    }
                    if (rightY >= r.getY() && rightY <= r.getY() + r.getHeight() && rightX >= r.getX() && rightX <= r.getX() + r.getWidth()) {
                        interRight = true;
                        TANK_ENEMY.get(i).setCollideRight(true);

                    }
                }
            }


            Rectangle r = tankRec;
            r.setBounds((int) (r.getX() + xScroll), (int) (r.getY() + yScroll), (int) r.getWidth(), (int) r.getHeight());

            if (upY >= r.getY() && upY <= r.getY() + r.getHeight() && upX >= r.getX() && upX <= r.getX() + r.getWidth()) {
                interUp = true;
                TANK_ENEMY.get(i).setCollideUP(true);


            }

            if (downY >= r.getY() && downY <= r.getY() + r.getHeight() && downX >= r.getX() && downX <= r.getX() + r.getWidth()) {
                interDown = true;
                TANK_ENEMY.get(i).setCollideDown(true);
            }
            if (leftY >= r.getY() && leftY <= r.getY() + r.getHeight() && leftX >= r.getX() && leftX <= r.getX() + r.getWidth()) {
                interLeft = true;
                TANK_ENEMY.get(i).setCollideLeft(true);

            }
            if (rightY >= r.getY() && rightY <= r.getY() + r.getHeight() && rightX >= r.getX() && rightX <= r.getX() + r.getWidth()) {
                interRight = true;
                TANK_ENEMY.get(i).setCollideRight(true);

            }
            r.setBounds((int) (r.getX() - xScroll), (int) (r.getY() - yScroll), (int) r.getWidth(), (int) r.getHeight());

            /**
             * move the enemy tanks
             */
            if (TANK_ENEMY.get(i).isActive()) {


                if (TANK_ENEMY.get(i).moveTime + 3000 < System.currentTimeMillis()) {

                    TANK_ENEMY.get(i).targetX = (TANK_ENEMY.get(i).firstX - 300 + 5 * random.nextInt(140));
                    TANK_ENEMY.get(i).targetY = (TANK_ENEMY.get(i).firstY - 300 + 5 * random.nextInt(140));////////////////////ohiho
                    TANK_ENEMY.get(i).moveTime = System.currentTimeMillis();
                }
                if (TANK_ENEMY.get(i).getX() > TANK_ENEMY.get(i).targetX && !TANK_ENEMY.get(i).isCollideLeft()) {
                    TANK_ENEMY.get(i).setX(TANK_ENEMY.get(i).getX() - 5);
                    TANK_ENEMY.get(i).moveLeft = true;
                } else {
                    TANK_ENEMY.get(i).moveLeft = false;
                }

                if (TANK_ENEMY.get(i).getX() < TANK_ENEMY.get(i).targetX && !TANK_ENEMY.get(i).isCollideRight()) {
                    TANK_ENEMY.get(i).setX(TANK_ENEMY.get(i).getX() + 5);
                    TANK_ENEMY.get(i).moveRight = true;
                } else {
                    TANK_ENEMY.get(i).moveRight = false;
                }

                if (TANK_ENEMY.get(i).getY() > TANK_ENEMY.get(i).targetY && !TANK_ENEMY.get(i).isCollideUP()) {
                    TANK_ENEMY.get(i).setY(TANK_ENEMY.get(i).getY() - 5);
                    TANK_ENEMY.get(i).moveUp = true;
                } else {
                    TANK_ENEMY.get(i).moveUp = false;
                }
                if (TANK_ENEMY.get(i).getY() < TANK_ENEMY.get(i).targetY && !TANK_ENEMY.get(i).isCollideDown()) {
                    TANK_ENEMY.get(i).setY(TANK_ENEMY.get(i).getY() + 5);
                    TANK_ENEMY.get(i).moveDown = true;
                } else {
                    TANK_ENEMY.get(i).moveDown = false;
                }


                if (TANK_ENEMY.get(i).moveUp && !TANK_ENEMY.get(i).moveDown) {
                    TANK_ENEMY.get(i).gama = -Math.PI / 2;
                    if (TANK_ENEMY.get(i).moveRight)
                        TANK_ENEMY.get(i).gama += Math.PI / 4;
                    if (TANK_ENEMY.get(i).moveLeft)
                        TANK_ENEMY.get(i).gama -= Math.PI / 4;
                }
                if (TANK_ENEMY.get(i).moveDown && !TANK_ENEMY.get(i).moveUp) {
                    TANK_ENEMY.get(i).gama = Math.PI / 2;
                    if (TANK_ENEMY.get(i).moveRight)
                        TANK_ENEMY.get(i).gama -= Math.PI / 4;
                    if (TANK_ENEMY.get(i).moveLeft)
                        TANK_ENEMY.get(i).gama += Math.PI / 4;
                }
                if (TANK_ENEMY.get(i).moveRight && !TANK_ENEMY.get(i).moveLeft) {
                    TANK_ENEMY.get(i).gama = 0;
                    if (TANK_ENEMY.get(i).moveUp)
                        TANK_ENEMY.get(i).gama -= Math.PI / 4;
                    if (TANK_ENEMY.get(i).moveDown)
                        TANK_ENEMY.get(i).gama += Math.PI / 4;
                }
                if (TANK_ENEMY.get(i).moveLeft && !TANK_ENEMY.get(i).moveRight) {
                    TANK_ENEMY.get(i).gama = Math.PI;
                    if (TANK_ENEMY.get(i).moveUp)
                        TANK_ENEMY.get(i).gama += Math.PI / 4;
                    if (TANK_ENEMY.get(i).moveDown)
                        TANK_ENEMY.get(i).gama -= Math.PI / 4;
                }
                if (TANK_ENEMY.get(i).gama - TANK_ENEMY.get(i).theta > Math.PI / 2) {
                    TANK_ENEMY.get(i).gama -= Math.PI;
                }
                if (TANK_ENEMY.get(i).theta - TANK_ENEMY.get(i).gama > Math.PI / 2) {
                    TANK_ENEMY.get(i).gama += Math.PI;
                }


                if ((TANK_ENEMY.get(i).moveUp || TANK_ENEMY.get(i).moveDown || TANK_ENEMY.get(i).moveRight || TANK_ENEMY.get(i).moveLeft) && ((TANK_ENEMY.get(i).theta - TANK_ENEMY.get(i).gama) != Math.PI) && ((TANK_ENEMY.get(i).theta - TANK_ENEMY.get(i).gama) != -Math.PI)) {

                    if (TANK_ENEMY.get(i).theta > TANK_ENEMY.get(i).gama)
                        TANK_ENEMY.get(i).theta -= Math.toRadians(8);
                    if (TANK_ENEMY.get(i).theta < TANK_ENEMY.get(i).gama)
                        TANK_ENEMY.get(i).theta += Math.toRadians(8);
                    if (((TANK_ENEMY.get(i).theta - TANK_ENEMY.get(i).gama) < Math.toRadians(8)) && TANK_ENEMY.get(i).theta > TANK_ENEMY.get(i).gama)
                        TANK_ENEMY.get(i).theta = TANK_ENEMY.get(i).gama;
                    if (((TANK_ENEMY.get(i).gama - TANK_ENEMY.get(i).theta) < Math.toRadians(8)) && TANK_ENEMY.get(i).theta < TANK_ENEMY.get(i).gama)
                        TANK_ENEMY.get(i).theta = TANK_ENEMY.get(i).gama;
                }


                if (TANK_ENEMY.get(i).fireTime + 500 < System.currentTimeMillis()) {
                    missiles.add(new Missile(TANK_ENEMY.get(i).x + GameFrame.enemyTank.getWidth() / 2 + GameFrame.enemyTankGun.getWidth() / 2 - xScroll
                            , TANK_ENEMY.get(i).y + GameFrame.enemyTank.getHeight() / 2 - yScroll
                            , TANK_ENEMY.get(i).alpha, BULLETS_ACTIVE,
                            TANK_ENEMY.get(i).x + GameFrame.enemyTank.getWidth() / 2 - xScroll,
                            TANK_ENEMY.get(i).y + GameFrame.enemyTank.getHeight() / 2 - yScroll, 0, 0, true, false)); //////////////

///nhonho

                    TANK_ENEMY.get(i).fireTime = System.currentTimeMillis();
                }
            }


        }


        /**
         * to check tank collision with different objects
         */


        upX = tankRec.x + tankRec.getWidth() / 2 + GameState.xScroll;
        upY = tankRec.y + GameState.yScroll;
        downX = tankRec.x + tankRec.getWidth() / 2 + GameState.xScroll;
        downY = tankRec.y + tankRec.getHeight() + GameState.yScroll;
        leftX = tankRec.x + GameState.xScroll;
        leftY = tankRec.y + tankRec.getHeight() / 2 + GameState.yScroll;
        rightX = tankRec.x + tankRec.getWidth() + GameState.xScroll;
        rightY = tankRec.y + tankRec.getHeight() / 2 + GameState.yScroll;


        interRight = false;
        interLeft = false;
        interUp = false;
        interDown = false;
        for (Rectangle r : bricks
                ) {
            if (true) {
                if (upY >= r.getY() && upY <= r.getY() + r.getHeight() && upX >= r.getX() && upX <= r.getX() + r.getWidth()) {
                    interUp = true;

                }
                if (downY >= r.getY() && downY <= r.getY() + r.getHeight() && downX >= r.getX() && downX <= r.getX() + r.getWidth()) {
                    interDown = true;
                }
                if (leftY >= r.getY() && leftY <= r.getY() + r.getHeight() && leftX >= r.getX() && leftX <= r.getX() + r.getWidth()) {
                    interLeft = true;

                }
                if (rightY >= r.getY() && rightY <= r.getY() + r.getHeight() && rightX >= r.getX() && rightX <= r.getX() + r.getWidth()) {
                    interRight = true;

                }


            }
        }

        Rectangle i = finish.getBounds(GameFrame.finish);

        if (upY >= i.getY() && upY <= i.getY() + i.getHeight() && upX >= i.getX() && upX <= i.getX() + i.getWidth()) {
            // GameFrame.time = System.currentTimeMillis();
            isFinished = true;
            NetworkClass.closeConnection();

        }
        if (downY >= i.getY() && downY <= i.getY() + i.getHeight() && downX >= i.getX() && downX <= i.getX() + i.getWidth()) {
            // GameFrame.time = System.currentTimeMillis();
            isFinished = true;
            NetworkClass.closeConnection();
        }
        if (leftY >= i.getY() && leftY <= i.getY() + i.getHeight() && leftX >= i.getX() && leftX <= i.getX() + i.getWidth()) {
            //GameFrame.time = System.currentTimeMillis();
            isFinished = true;
            NetworkClass.closeConnection();

        }
        if (rightY >= i.getY() && rightY <= i.getY() + i.getHeight() && rightX >= i.getX() && rightX <= i.getX() + i.getWidth()) {
            //  GameFrame.time = System.currentTimeMillis();
            isFinished = true;
            NetworkClass.closeConnection();

        }


        for (Rectangle r : tankEnemiesRec
                ) {
            if (true) {
                if (upY >= r.getY() && upY <= r.getY() + r.getHeight() && upX >= r.getX() && upX <= r.getX() + r.getWidth()) {
                    interUp = true;

                }
                if (downY >= r.getY() && downY <= r.getY() + r.getHeight() && downX >= r.getX() && downX <= r.getX() + r.getWidth()) {
                    interDown = true;
                }
                if (leftY >= r.getY() && leftY <= r.getY() + r.getHeight() && leftX >= r.getX() && leftX <= r.getX() + r.getWidth()) {
                    interLeft = true;

                }
                if (rightY >= r.getY() && rightY <= r.getY() + r.getHeight() && rightX >= r.getX() && rightX <= r.getX() + r.getWidth()) {
                    interRight = true;

                }


            }
        }
        for (Rectangle r : idiotEnemiesRec
                ) {
            if (true) {
                if (upY >= r.getY() && upY <= r.getY() + r.getHeight() && upX >= r.getX() && upX <= r.getX() + r.getWidth()) {
                    interUp = true;

                }
                if (downY >= r.getY() && downY <= r.getY() + r.getHeight() && downX >= r.getX() && downX <= r.getX() + r.getWidth()) {
                    interDown = true;
                }
                if (leftY >= r.getY() && leftY <= r.getY() + r.getHeight() && leftX >= r.getX() && leftX <= r.getX() + r.getWidth()) {
                    interLeft = true;

                }
                if (rightY >= r.getY() && rightY <= r.getY() + r.getHeight() && rightX >= r.getX() && rightX <= r.getX() + r.getWidth()) {
                    interRight = true;

                }


            }
        }
        for (Rectangle r : fixedTankRec
                ) {
            if (true) {
                if (upY >= r.getY() && upY <= r.getY() + r.getHeight() && upX >= r.getX() && upX <= r.getX() + r.getWidth()) {
                    interUp = true;

                }
                if (downY >= r.getY() && downY <= r.getY() + r.getHeight() && downX >= r.getX() && downX <= r.getX() + r.getWidth()) {
                    interDown = true;
                }
                if (leftY >= r.getY() && leftY <= r.getY() + r.getHeight() && leftX >= r.getX() && leftX <= r.getX() + r.getWidth()) {
                    interLeft = true;

                }
                if (rightY >= r.getY() && rightY <= r.getY() + r.getHeight() && rightX >= r.getX() && rightX <= r.getX() + r.getWidth()) {
                    interRight = true;

                }


            }
        }
        for (Rectangle r : launcherRec
                ) {
            if (true) {
                if (upY >= r.getY() && upY <= r.getY() + r.getHeight() && upX >= r.getX() && upX <= r.getX() + r.getWidth()) {
                    interUp = true;

                }
                if (downY >= r.getY() && downY <= r.getY() + r.getHeight() && downX >= r.getX() && downX <= r.getX() + r.getWidth()) {
                    interDown = true;
                }
                if (leftY >= r.getY() && leftY <= r.getY() + r.getHeight() && leftX >= r.getX() && leftX <= r.getX() + r.getWidth()) {
                    interLeft = true;

                }
                if (rightY >= r.getY() && rightY <= r.getY() + r.getHeight() && rightX >= r.getX() && rightX <= r.getX() + r.getWidth()) {
                    interRight = true;

                }


            }
        }

        for (Rectangle r : softBricksRec
                ) {
            if (true) {
                if (upY >= r.getY() && upY <= r.getY() + r.getHeight() && upX >= r.getX() && upX <= r.getX() + r.getWidth()) {
                    interUp = true;

                }
                if (downY >= r.getY() && downY <= r.getY() + r.getHeight() && downX >= r.getX() && downX <= r.getX() + r.getWidth()) {
                    interDown = true;
                }
                if (leftY >= r.getY() && leftY <= r.getY() + r.getHeight() && leftX >= r.getX() && leftX <= r.getX() + r.getWidth()) {
                    interLeft = true;

                }
                if (rightY >= r.getY() && rightY <= r.getY() + r.getHeight() && rightX >= r.getX() && rightX <= r.getX() + r.getWidth()) {
                    interRight = true;

                }


            }
        }

        /**
         * functonality of foods
         */
        for (Sprite s : FOOD
                ) {
            Rectangle r = s.getBounds(GameFrame.upgradeFood);
            r.setBounds((int) (r.getX() - xScroll), (int) (r.getY() - yScroll), (int) r.getWidth(), (int) r.getHeight());
            if (tankRec.intersects(r)) {
                if (s.getType() == Sprite.FOOD_G) {
                    Sound.playSound("src/sources/eat.wav");
                    tank.setBulletsLeft(tank.getBulletsLeft() + 80);
                    s.setX(-200);
                    s.setY(-200);
                }
                if (s.getType() == Sprite.FOOD_M) {
                    Sound.playSound("src/sources/eat.wav");
                    tank.setMissilesLeft((tank.getMissilesLeft() + 40));
                    s.setX(-200);
                    s.setY(-200);
                }
                if (s.getType() == Sprite.FOOD_REPAIR) {
                    Sound.playSound("src/sources/eat.wav");
                    tank.setHitPoint(5);
                    s.setX(-200);
                    s.setY(-200);
                }
                if (s.getType() == Sprite.FOOD_UPGRADE) {
                    Sound.playSound("src/sources/eat.wav");
                    if (Tank.activeGun == Tank.MISSILE) {
                        Missile.missileSpeed += 3;
                        Missile.powerM += 0.2;
                        s.setX(-200);
                        s.setY(-200);
                    } else {

                        Missile.bulletSpeed += 3;
                        Missile.powerB += 0.05;
                        s.setX(-200);
                        s.setY(-200);
                    }
                }

            }
            r.setBounds((int) (r.getX() + xScroll), (int) (r.getY() + yScroll), (int) r.getWidth(), (int) r.getHeight());
        }


        /**
         * move the tank if !collide
         */
        Rectangle in = new Rectangle();
        if (keyUP) {
            if (!interUp) {
                tank.y -= 7;
                interUp = false;
            }
        }
        if (keyDOWN) {

            if (!interDown) {
                tank.y += 7;
                interDown = false;
            }
        }


        if (keyLEFT) {
            if (!interLeft) {
                tank.x -= 7;
            }


        }

        if (keyRIGHT) {
            if (!interRight) {
                tank.x += 7;
            }
        }

        /**
         * rotate tank
         */
        if ((keyRIGHT || keyLEFT || keyUP || keyDOWN) && ((tank.theta - tank.gama) != Math.PI) && ((tank.theta - tank.gama) != -Math.PI)) {
            Sound.playSound("src/sources/move.wav");
            if (tank.theta > tank.gama)
                tank.theta -= Math.toRadians(8);
            if (tank.theta < tank.gama)
                tank.theta += Math.toRadians(8);
            if (((tank.theta - tank.gama) < Math.toRadians(8)) && tank.theta > tank.gama)
                tank.theta = tank.gama;
            if (((tank.gama - tank.theta) < Math.toRadians(8)) && tank.theta < tank.gama)
                tank.theta = tank.gama;
        }

        /**
         * move map up and down
         */
        if (tank.y > GameFrame.GAME_HEIGHT * 3 / 5) {
            GameFrame.transY -= 7;
            yScroll += 7;
            tank.y -= 7;
            for (Missile ms : missiles) {
                ms.y -= 7;
                ms.yC -= 7;
                // ms.yR += 7;
                //ms.y -= 7;
            }

        }
        /**
         * move map right or left
         */
        if (tank.x > GameFrame.GAME_WIDTH * 3 / 5) {
            GameFrame.transX -= 7;
            xScroll += 7;
            tank.x -= 7;
            for (Missile ms : missiles) {
                ms.x -= 7;
                ms.xC -= 7;
                // ms.yR += 7;
                //ms.y -= 7;
            }

        }
        if (tank.y < GameFrame.GAME_HEIGHT * 2 / 5 && yScroll > 0) {
            GameFrame.transY += 7;
            yScroll -= 7;
            tank.y += 7;
            for (Missile ms : missiles) {
                ms.y += 7;
                ms.yC += 7;
                //ms.yR -= 7;
            }
        }
        if (tank.x < GameFrame.GAME_WIDTH * 2 / 5 && xScroll > 0) {
            GameFrame.transX += 7;
            xScroll -= 7;
            tank.x += 7;
            for (Missile ms : missiles) {
                ms.x += 7;
                ms.xC += 7;
                //ms.yR -= 7;
            }
        }
        for (Explosion ex : explosions) {
            if (ex.time + ex.duration < System.currentTimeMillis()) {
                ex.active = false;
            }
        }


        /**
         * to set game over true
         */
        if (tank.getHitPoint() <= 0) {
            //gameOver = true;
            loose = true;

            if (looseTimec == 0) {
                looseTime = System.currentTimeMillis();

                looseTimec++;
            }
            if (looseTime + 3000 < System.currentTimeMillis())
                gameOver = true;

        }
        if (GameState.missiles.size() > 200) {
            ArrayList<Missile> ms = new ArrayList<>(GameState.missiles);
            GameState.missiles = new ArrayList<>();
            for (int j = 100; j < ms.size(); j++) {
                if (ms.get(j).xC != -42000) {
                    GameState.missiles.add(ms.get(j));
                }
            }
        }
    }


    public KeyListener getKeyListener() {
        return keyHandler;
    }

    public MouseListener getMouseListener() {
        return mouseHandler;
    }

    public MouseMotionListener getMouseMotionListener() {
        return mouseHandler;
    }


    /**
     * The keyboard handler.
     */
    class KeyHandler extends KeyAdapter {
        @Override
        public void keyTyped(KeyEvent e) {
            if (e.getKeyCode() != KeyEvent.VK_A && e.getKeyCode() != KeyEvent.VK_S && e.getKeyCode() != KeyEvent.VK_D && e.getKeyCode() != KeyEvent.VK_W) {


                if (System.currentTimeMillis() - chtTime > 1000) {
                    cheat = "";
                }
                cheat = cheat + e.getKeyChar();
                chtTime = System.currentTimeMillis();
                if (cheat.toLowerCase().equals("fullb")) {
                    // System.out.println(":))");
                    tank.setBulletsLeft(200);
                    cheat = "";
                }
                if (cheat.toLowerCase().equals("fullm")) {
                    // System.out.println(":))");
                    tank.setMissilesLeft(100);
                    cheat = "";
                }
                if (cheat.toLowerCase().equals("morem")) {
                    // System.out.println(":))");
                    tank.setMissilesLeft(tank.getMissilesLeft() + 50);
                    cheat = "";
                }
                if (cheat.toLowerCase().equals("ilive")) {
                    // System.out.println(":))");
                    tank.setHitPoint(5);
                    cheat = "";
                }

                if (cheat.toLowerCase().equals("moreb")) {
                    // System.out.println(":))");
                    tank.setBulletsLeft(tank.getBulletsLeft() + 50);
                    cheat = "";
                }
                // System.out.println(cheat);
            }
        }

        @Override
        public void keyPressed(KeyEvent e) {
            switch (e.getKeyCode()) {
                case KeyEvent.VK_UP:
                    keyUP = true;
                    break;
                case KeyEvent.VK_DOWN:
                    keyDOWN = true;
                    break;
                case KeyEvent.VK_LEFT:
                    keyLEFT = true;
                    break;
                case KeyEvent.VK_RIGHT:
                    keyRIGHT = true;
                    break;
                case KeyEvent.VK_W:
                    keyUP = true;
                    break;
                case KeyEvent.VK_S:
                    keyDOWN = true;
                    break;
                case KeyEvent.VK_A:
                    keyLEFT = true;
                    break;
                case KeyEvent.VK_D:
                    keyRIGHT = true;
                    break;
                case KeyEvent.VK_ESCAPE:
                    System.exit(100);
                    break;


            }
            if (e.getKeyCode() == KeyEvent.VK_S && e.isControlDown()) {
                NetworkClass.saveGame();
            } else if (e.getKeyCode() == KeyEvent.VK_L && e.isControlDown()) {
                NetworkClass.loadGame();
            } else if (e.getKeyCode() == KeyEvent.VK_P && e.isControlDown()) {
                if (GameState.isPaused) {
                    GameState.isPaused = false;
                } else {
                    GameState.isPaused = true;
                }

            }


        }

        @Override
        public void keyReleased(KeyEvent e) {
            switch (e.getKeyCode()) {
                case KeyEvent.VK_UP:
                    keyUP = false;
                    break;
                case KeyEvent.VK_DOWN:
                    keyDOWN = false;
                    break;
                case KeyEvent.VK_LEFT:
                    keyLEFT = false;
                    break;
                case KeyEvent.VK_RIGHT:
                    keyRIGHT = false;
                    break;
                case KeyEvent.VK_W:
                    keyUP = false;
                    break;
                case KeyEvent.VK_S:
                    keyDOWN = false;
                    break;
                case KeyEvent.VK_A:
                    keyLEFT = false;
                    break;
                case KeyEvent.VK_D:
                    keyRIGHT = false;
                    break;
            }
        }

    }

    /**
     * The mouse handler.
     */
    class MouseHandler extends MouseAdapter {

        @Override
        public void mousePressed(MouseEvent e) {
            if (e.getButton() == MouseEvent.BUTTON3) {
                if (tank.getActiveGun() == Tank.MISSILE) {
                    tank.setActiveGun(Tank.MACHINE_GUN);

                } else
                    tank.setActiveGun(Tank.MISSILE);

                tank.activeGunn = Tank.activeGun;

            } else {
                mouseX = e.getX() + GameFrame.cur.getIconWidth() / 2;
                mouseY = e.getY() + GameFrame.cur.getIconHeight() / 2;
                mousePress = true;
            }
        }


        @Override
        public void mouseReleased(MouseEvent e) {
            mousePress = false;
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            mouseX = e.getX() + GameFrame.cur.getIconWidth() / 2;
            mouseY = e.getY() + GameFrame.cur.getIconHeight() / 2;
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            mouseX = e.getX() + GameFrame.cur.getIconWidth() / 2;
            mouseY = e.getY() + GameFrame.cur.getIconHeight() / 2;
        }
    }
}

