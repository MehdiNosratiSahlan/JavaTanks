import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;


/**
 * to read the map file and handle game difficulty
 */
public class MapUtils {

    public static int difficulty = 0;



    Scanner scanner;
      File mapFile;
    public static ArrayList<String> map = new ArrayList<>();


    public MapUtils() {

        if(difficulty == 0){
            mapFile = new File("src/sources/map-easy.txt");
        }else if (difficulty == 1){
            mapFile = new File("src/sources/map-medium.txt");
        }
        else if (difficulty == 2){
            mapFile = new File("src/sources/map-hard.txt");
        }
        else if (difficulty == 3){
            mapFile = Settings.mapFile;
        }


        {
            try {
                scanner = new Scanner(mapFile);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            while (scanner.hasNext()) {
                map.add(scanner.nextLine());
            }
        }


    }

}
