import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * for network connection and data streaming
 *
 * Created by Saeid Ghahremannejad on 7/14/2018.
 */
public class networkObjects implements Serializable{



        public Tank tank;
        public boolean paused;
        public ArrayList<Enemy> tankEnemies;
        public ArrayList<Enemy> idiotEnemies;
        public ArrayList<Missile> missiles;

        public int xScroll,yScroll;

        public networkObjects(){
            tank = GameState.tank;
            paused = GameState.isPaused;
            tankEnemies = new ArrayList<>(GameState.TANK_ENEMY);
            missiles = new ArrayList<>(GameState.missiles);
            idiotEnemies = new ArrayList<>(GameState.IDIOT_ENEMY);
            xScroll = GameState.xScroll;
            yScroll = GameState.yScroll;

        }


}
