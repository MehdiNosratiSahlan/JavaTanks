import java.io.Serializable;

/**
 * explosion effects of game
 * Created by Saeid Ghahremannejad on 7/11/2018.
 */
public class Explosion implements Serializable{
    int x ,y;
    long time;
    boolean active;
    public  int duration = 30;
    int type;

    public Explosion(int x, int y , int type){
        this.x = x;
        this.y = y;
        time = System.currentTimeMillis();
        active = true;
        this.type = type;
        if (type == GameState.BULLETS_ACTIVE){
            duration = 30;
        }
        if (type == GameState.MISSILE_ACTIVE){
            duration = 100;
        }
    }
}
